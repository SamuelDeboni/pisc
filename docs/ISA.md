# PISC

## 16-bit instruction set Stack Machine

### 16-bit instruction

| Instruction | Opcode | I  |
|-------------|--------|----|
| PUSH        | 0xF0   | XX |
| PUSHI       | 0xF1   | XX |
| POP         | 0xF2   | XX |
| JNQ         | 0xF3   | XX |
| JEQ         | 0xF4   | XX |

### 8-bit instructions

| Instruction | Opcode |
|-------------|--------|
| NOP         | 0x00   |
| ADD         | 0x01   |
| SUB         | 0x02   |
| MUL         | 0x03   |
| DIV         | 0x04   |
| OR          | 0x05   |
| AND         | 0x06   |
| XOR         | 0x07   |
| NOT         | 0x08   |
| SL          | 0x09   |
| SRA         | 0x0A   |
| SRL         | 0x0B   |
| CMP         | 0x0C   |
| JMP         | 0x0D   |
| LOAD        | 0x0E   |
| STORE       | 0x0F   |
| INC         | 0x10   |
| DEC         | 0x11   |

### Pseudo Instructions

- PUSH LABEL
```as
push  LABEL_LOW
pushi 0x08
push  LABEL_HIGH
sl
add
```

### Instructions documentation

#### PUSH

Increment the stack pointer and write the value in to the stack

#### POP

Decrement the stack pointer

#### NOP

No operation, does nothing

#### ADD / SUB / MUL / DIV

Aritimetic operations

Pop two values of the stack, do the operation, and push the result to the stack

#### OR / AND / XOR

Bitwise operations

Pop two values of the stack, do the operaiton, and push the result to the stack

#### NOT

Pop the top value of the stack, and push the bitwise not on the stack

#### SLA / SLL / SR

- SLA = Shift Left aritimetic
- SLL = Shift right aritimetic
- SR  = Shift right

#### JMP

Pop the stack
Jump to the address of the value

#### JEQ

Pop the stack, if the value is equal to the imediate
Pop the stack and jump the the address

#### JNQ

Pop the stack, if the value is not equal to the imediate
Pop the stack and jump the the address
