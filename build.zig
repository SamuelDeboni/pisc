const std = @import("std");
const builtin = @import("builtin");


const packages = [_]std.build.Pkg {
    .{
        .name = "ztt",
        .path = "packages/zig_truetype/zig_truetype.zig",
    },
};

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    
    const mode = b.standardReleaseOptions();
    
    const windows = b.option(bool, "windows", "create windows build") orelse false;
    const strip = b.option(bool, "strip", "strip debug info") orelse false;
    
    var exe = b.addExecutable("pisc", "src/glfw_plataform.zig");
    
    for (packages) |pack| {
        exe.addPackage(pack);
    }
    
    exe.addCSourceFile("packages/zig_truetype/stb_truetype.c", &[_][]const u8{"-std=c99"});
    exe.setTarget(target);
    
    if (windows or builtin.os.tag == .windows) {
        exe.setTarget
            (.{
                 .cpu_arch = .x86_64,
                 .os_tag = .windows,
                 .abi = .gnu,
             });
    }
    
    if (strip) {
        exe.strip = true;
    }
    
    exe.setBuildMode(mode);
    exe.linkLibC();
    
    if (windows or builtin.os.tag == .windows) {
        exe.linkSystemLibrary("gdi32");
        exe.linkSystemLibrary("shell32");
        exe.linkSystemLibrary("kernel32");
        //exe.linkSystemLibrary("ws2_32");
        //exe.linkSystemLibrary("winmm");
        exe.addLibPath("lib");
    }
    
    exe.addIncludeDir("include");
    exe.addIncludeDir("packages/zig_truetype");
    exe.linkSystemLibrary("m");
    exe.linkSystemLibrary("glfw3");
    
    
    exe.install();
    
    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
