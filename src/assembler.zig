usingnamespace @import("index.zig");

pub const AssemblyStatus = struct {
    msg: []const u8 = "(nil)",
    output: []const u8 = undefined,
    debug: []const u8 = undefined,
    ok: bool,
};

const LabelInfo = struct {
    name: []const u8,
    name_addr: u32,
    addr: u64,
};

pub const DebugHeader = packed struct {
    entry_qnt: u32,
    data_offset: u32,
};

const DebugEntryType = packed enum(u32) {
    label,
    pseudo,
};

pub const DebugEntry = packed struct {
    addr: u64,
    data: u32,
    meaning: DebugEntryType,
    pseudo_end: u64 = 0,
};

const assembly_lexer_table = @embedFile("assembly_ifrexp.bin");

fn get_instruction_param_qnt(op: u8) u8 {
    switch (op) {
        ops.push => return 1,
        ops.pushi => return 1,
        ops.pop => return 1,
        ops.jne => return 1,
        ops.jeq => return 1,
        else => return 0,
    }
}

fn mnemonic_to_bin(s: []const u8) !u8 {

    if (string.cmp(s, "push")) {
        return ops.push;
    } else if (string.cmp(s, "pushi")) {
        return ops.pushi;
    } else if (string.cmp(s, "pop")) {
        return ops.pop;
    } else if (string.cmp(s, "jne")) {
        return ops.jne;
    } else if (string.cmp(s, "jeq")) {
        return ops.jeq;
    } else if (string.cmp(s, "nop")) {
        return ops.nop;
    } else if (string.cmp(s, "add")) {
        return ops.add;
    } else if (string.cmp(s, "sub")) {
        return ops.sub;
    } else if (string.cmp(s, "mul")) {
        return ops.mul;
    } else if (string.cmp(s, "div")) {
        return ops.div;
    } else if (string.cmp(s, "or")) {
        return ops._or;
    } else if (string.cmp(s, "and")) {
        return ops._and;
    } else if (string.cmp(s, "xor")) {
        return ops.xor;
    } else if (string.cmp(s, "not")) {
        return ops._not;
    } else if (string.cmp(s, "sl")) {
        return ops.sl;
    } else if (string.cmp(s, "sra")) {
        return ops.sra;
    } else if (string.cmp(s, "srl")) {
        return ops.srl;
    } else if (string.cmp(s, "cmp")) {
        return ops.cmp;
    } else if (string.cmp(s, "jmp")) {
        return ops.jmp;
    } else if (string.cmp(s, "load")) {
        return ops.load;
    } else if (string.cmp(s, "store")) {
        return ops.store;
    } else if (string.cmp(s, "inc")) {
        return ops.inc;
    } else if (string.cmp(s, "dec")) {
        return ops.dec;
    }

    return error.NotFound;
}

pub fn assembly(src: []const u8, al: *Allocator) AssemblyStatus {
    var status = AssemblyStatus{
        .ok = false,
    };

    var debug_entries = al.alloc(DebugEntry, 4096) catch {
        status.msg = "Unable to alloc debug entries table";
        return status;
    };
    var debug_entry_qnt: usize = 0;

    var debug_data_buffer = string.create_builder(al) catch {
        status.msg = "Unable to alloc debug data buffer";
        return status;
    };

    var labels = al.alloc(LabelInfo, 4096) catch {
        status.msg = "Unable to alloc labels info table";
        return status;
    };
    var label_qnt: usize = 0;

    // extract labels addresses

    var ctx = LexerContext{
        .table = assembly_lexer_table,
        .source = src,
    };

    var pc: usize = 0;
    while (true) {
        var pair = get_token(&ctx) catch |e| {
            if (e == LexerError.EndOfSource) break;

            status.msg = "Error in get token";
            return status;
        };  

        if (pair.meaning == .new_line) continue;

        if (pair.meaning == .label_decl) {
            labels[label_qnt].name = pair.token[0..(pair.token.len - 1)];
            labels[label_qnt].addr = pc;
            labels[label_qnt].name_addr = @intCast(u32, debug_data_buffer.len);

            string.builder_append(&debug_data_buffer, pair.token, true) catch {
                status.msg = "Allocation error";
                return status;
            };

            string.builder_append_byte(&debug_data_buffer, 0, true) catch {
                status.msg = "Allocation error";
                return status;
            };

            debug_entries[debug_entry_qnt].addr = pc;
            debug_entries[debug_entry_qnt].data = labels[label_qnt].name_addr;
            debug_entries[debug_entry_qnt].meaning = .label;

            label_qnt += 1;
            debug_entry_qnt += 1;

            pair = get_token(&ctx) catch |e| {
                if (e == LexerError.EndOfSource) break;

                status.msg = "Error in get token";
                return status;
            };

            if (pair.meaning != .new_line) {
                status.msg = "You cannot put label declaration and another things in same line";
                return status;
            }

            continue;
        }

        if (string.cmp(pair.token, "@pushl")) {
            pair = get_token(&ctx) catch {
                status.msg = "Error in get parameter token";
                return status;
            };

            if (pair.meaning != .label) {
                status.msg = "Label required";
                return status;
            }

            pc += 8;

            continue;
        }

        const op = mnemonic_to_bin(pair.token) catch {
            status.msg = "Instruction mnemonic not found";
            return status;
        };

        pc += 1;

        const param_qnt = get_instruction_param_qnt(op);
        var param_i: usize = 0;
        while (param_i < param_qnt) : (param_i += 1) {
            pair = get_token(&ctx) catch {
                status.msg = "Error in get parameter token";
                return status;
            };

            switch (pair.meaning) {
                .integer => pc += 1,
                .label => pc += 1,
                else => {
                    status.msg = "Parameter must to be an integer or a label";
                    return status;
                },
            }
        }
    }

    // generate the output

    ctx = LexerContext{
        .table = assembly_lexer_table,
        .source = src,
    };

    var output = string.create_builder(al) catch {
        status.msg = "Unable to alloc output buffer";
        return status;
    };

    pc = 0;
    while (true) {
        var pair = get_token(&ctx) catch |e| {
            if (e == LexerError.EndOfSource) break;

            status.msg = "Error in get token";
            return status;
        };  

        if (pair.meaning == .new_line or pair.meaning == .label_decl) continue;

        if (string.cmp(pair.token, "@pushl")) {
            pair = get_token(&ctx) catch {
                status.msg = "Error in get parameter token";
                return status;
            };

            if (pair.meaning != .label) {
                status.msg = "Label required";
                return status;
            }

            var found_label = false;
            var label_addr: u16 = 0;
            var label_i: usize = 0;
            while (label_i < label_qnt) : (label_i += 1) {
                if (string.cmp(pair.token, labels[label_i].name)) {
                    label_addr = @intCast(u16, labels[label_i].addr);
                    found_label = true;
                    break;
                }
            }

            if (!found_label) {
                status.msg = "Label not found";
                return status;
            }

            var buffer: [8]u8 = undefined;

            buffer[0] = ops.pushi;
            buffer[1] = @intCast(u8, label_addr & 0x00ff);
            buffer[2] = ops.pushi;
            buffer[3] = 8;
            buffer[4] = ops.pushi;
            buffer[5] = @intCast(u8, label_addr & 0xff00);
            buffer[6] = ops.sl;
            buffer[7] = ops.add;

            string.builder_append(&output, buffer[0..8], true) catch {
                status.msg = "Allocation error";
                return status;
            };
            pc += 8;

            continue;
        }

        const op = mnemonic_to_bin(pair.token) catch {
            status.msg = "Instruction mnemonic not found";
            return status;
        };

        string.builder_append_byte(&output, op, true) catch {
            status.msg = "Allocation error";
            return status;
        };
        pc += 1;

        const param_qnt = get_instruction_param_qnt(op);
        var param_i: usize = 0;
        while (param_i < param_qnt) : (param_i += 1) {
            pair = get_token(&ctx) catch {
                status.msg = "Error in get parameter token";
                return status;
            };

            switch (pair.meaning) {
                .integer => {
                    const param = string.parse_u8(pair.token);

                    string.builder_append_byte(&output, param, true)  catch {
                        status.msg = "Allocation error";
                        return status;
                    };
                    pc += 1;
                },
                .label => {

                },
                else => {
                    status.msg = "Parameter must to be an integer";
                    return status;
                },
            }
        }
    }

    {
        var debug = string.create_builder(al) catch {
            status.msg = "Unable to alloc output buffer";
            return status;
        };

        const header = DebugHeader{
            .entry_qnt = @intCast(u32, debug_entry_qnt),
            .data_offset = @intCast(u32, @sizeOf(DebugHeader) + @sizeOf(DebugEntry)*debug_entry_qnt),
        };

        var tmp: []const u8 = undefined;

        tmp.ptr = @ptrCast([*]const u8, &header);
        tmp.len = @sizeOf(DebugHeader);

        string.builder_append(&debug, tmp, true) catch {
            status.msg = "Allocation error";
            return status;
        };

        var entry_i: usize = 0;
        while (entry_i < debug_entry_qnt) : (entry_i += 1) {
            tmp.ptr = @ptrCast([*]const u8, &debug_entries[entry_i]);
            tmp.len = @sizeOf(DebugEntry);

            string.builder_append(&debug, tmp, true) catch {
                status.msg = "Allocation error";
                return status;
            };
        }

        var debug_data = string.builder_gen_string_alloc(&debug_data_buffer, al) catch {
            status.msg = "Allocation error";
            return status;
        };

        string.builder_append(&debug, debug_data, true) catch {
            status.msg = "Allocation error";
            return status;
        };

        status.debug = string.builder_gen_string_alloc(&debug, al) catch {
            status.msg = "Allocation error";
            return status;
        };
    }

    {
        status.output = string.builder_gen_string_alloc(&output, al) catch {
            status.msg = "Allocation error";
            return status;
        };
    }

    status.ok = true;

    return status;
}