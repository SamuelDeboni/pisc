pub const main_file_path = "main.zig";

const std = @import("std");
const render = @import("dlib/render_opengl.zig");  
const input = @import("dlib/input.zig");


pub const Allocator = std.mem.Allocator;

pub var g_allocator: *Allocator = undefined;


usingnamespace @cImport
({
     @cDefine("GLFW_INCLUDE_NONE", {});
     @cInclude("GLFW/glfw3.h");
 });

pub const KeyState = enum(c_int) {
    press = GLFW_PRESS,
    release = GLFW_RELEASE,
};

pub const keymap = [_]c_int{
    GLFW_KEY_SPACE,
    GLFW_KEY_APOSTROPHE,
    GLFW_KEY_COMMA,
    GLFW_KEY_MINUS,
    GLFW_KEY_PERIOD,
    GLFW_KEY_SLASH,
    GLFW_KEY_0,
    GLFW_KEY_1,
    GLFW_KEY_2,
    GLFW_KEY_3,
    GLFW_KEY_4,
    GLFW_KEY_5,
    GLFW_KEY_6,
    GLFW_KEY_7,
    GLFW_KEY_8,
    GLFW_KEY_9,
    GLFW_KEY_SEMICOLON,
    GLFW_KEY_EQUAL,
    GLFW_KEY_A,
    GLFW_KEY_B,
    GLFW_KEY_C,
    GLFW_KEY_D,
    GLFW_KEY_E,
    GLFW_KEY_F,
    GLFW_KEY_G,
    GLFW_KEY_H,
    GLFW_KEY_I,
    GLFW_KEY_J,
    GLFW_KEY_K,
    GLFW_KEY_L,
    GLFW_KEY_M,
    GLFW_KEY_N,
    GLFW_KEY_O,
    GLFW_KEY_P,
    GLFW_KEY_Q,
    GLFW_KEY_R,
    GLFW_KEY_S,
    GLFW_KEY_T,
    GLFW_KEY_U,
    GLFW_KEY_V,
    GLFW_KEY_W,
    GLFW_KEY_X,
    GLFW_KEY_Y,
    GLFW_KEY_Z,
    GLFW_KEY_LEFT_BRACKET,
    GLFW_KEY_BACKSLASH,
    GLFW_KEY_RIGHT_BRACKET,
    GLFW_KEY_GRAVE_ACCENT,
    GLFW_KEY_WORLD_1,
    GLFW_KEY_WORLD_2,
    GLFW_KEY_ESCAPE,
    GLFW_KEY_ENTER,
    GLFW_KEY_TAB,
    GLFW_KEY_BACKSPACE,
    GLFW_KEY_INSERT,
    GLFW_KEY_DELETE,
    GLFW_KEY_RIGHT,
    GLFW_KEY_LEFT,
    GLFW_KEY_DOWN,
    GLFW_KEY_UP,
    GLFW_KEY_PAGE_UP,
    GLFW_KEY_PAGE_DOWN,
    GLFW_KEY_HOME,
    GLFW_KEY_END,
    GLFW_KEY_CAPS_LOCK,
    GLFW_KEY_SCROLL_LOCK,
    GLFW_KEY_NUM_LOCK,
    GLFW_KEY_PRINT_SCREEN,
    GLFW_KEY_PAUSE,
    GLFW_KEY_F1,
    GLFW_KEY_F2,
    GLFW_KEY_F3,
    GLFW_KEY_F4,
    GLFW_KEY_F5,
    GLFW_KEY_F6,
    GLFW_KEY_F7,
    GLFW_KEY_F8,
    GLFW_KEY_F9,
    GLFW_KEY_F10,
    GLFW_KEY_F11,
    GLFW_KEY_F12,
    GLFW_KEY_F13,
    GLFW_KEY_F14,
    GLFW_KEY_F15,
    GLFW_KEY_F16,
    GLFW_KEY_F17,
    GLFW_KEY_F18,
    GLFW_KEY_F19,
    GLFW_KEY_F20,
    GLFW_KEY_F21,
    GLFW_KEY_F22,
    GLFW_KEY_F23,
    GLFW_KEY_F24,
    GLFW_KEY_F25,
    GLFW_KEY_KP_0,
    GLFW_KEY_KP_1,
    GLFW_KEY_KP_2,
    GLFW_KEY_KP_3,
    GLFW_KEY_KP_4,
    GLFW_KEY_KP_5,
    GLFW_KEY_KP_6,
    GLFW_KEY_KP_7,
    GLFW_KEY_KP_8,
    GLFW_KEY_KP_9,
    GLFW_KEY_KP_DECIMAL,
    GLFW_KEY_KP_DIVIDE,
    GLFW_KEY_KP_MULTIPLY,
    GLFW_KEY_KP_SUBTRACT,
    GLFW_KEY_KP_ADD,
    GLFW_KEY_KP_ENTER,
    GLFW_KEY_KP_EQUAL,
    GLFW_KEY_LEFT_SHIFT,
    GLFW_KEY_LEFT_CONTROL,
    GLFW_KEY_LEFT_ALT,
    GLFW_KEY_LEFT_SUPER,
    GLFW_KEY_RIGHT_SHIFT,
    GLFW_KEY_RIGHT_CONTROL,
    GLFW_KEY_RIGHT_ALT,
    GLFW_KEY_RIGHT_SUPER,
    GLFW_KEY_MENU,
};


pub const Key = enum(usize) {
    space,
    apostrophe,
    comma,
    minus,
    period,
    slash,
    _0,
    _1,
    _2,
    _3,
    _4,
    _5,
    _6,
    _7,
    _8,
    _9,
    semicolon,
    equal,
    a,
    b,
    c,
    d,
    e,
    f,
    g,
    h,
    i,
    j,
    k,
    l,
    m,
    n,
    o,
    p,
    q,
    r,
    s,
    t,
    u,
    v,
    w,
    x,
    y,
    z,
    left_bracket,
    backslash,
    right_bracket,
    grave_accent,
    world_1,
    world_2,
    escape,
    enter,
    tab,
    backspace,
    insert,
    delete,
    right,
    left,
    down,
    up,
    page_up,
    page_down,
    home,
    end,
    caps_lock,
    scroll_lock,
    num_lock,
    print_screen,
    pause,
    f1,
    f2,
    f3,
    f4,
    f5,
    f6,
    f7,
    f8,
    f9,
    f10,
    f11,
    f12,
    f13,
    f14,
    f15,
    f16,
    f17,
    f18,
    f19,
    f20,
    f21,
    f22,
    f23,
    f24,
    f25,
    kp_0,
    kp_1,
    kp_2,
    kp_3,
    kp_4,
    kp_5,
    kp_6,
    kp_7,
    kp_8,
    kp_9,
    kp_decimal,
    kp_divide,
    kp_multiply,
    kp_subtract,
    kp_add,
    kp_enter,
    kp_equaL,
    left_shift,
    left_control,
    left_alt,
    left_super,
    right_shift,
    right_controL,
    right_alt,
    right_super,
    menu,
};

pub const Window = struct {
    width: i64 = 0,
    height: i64 = 0,
    is_fullscreen: bool = false,
    handle: *GLFWwindow = undefined,
};

pub var window: Window = .{};


pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    //defer _ = gpa.deinit();
    g_allocator = &gpa.allocator;
    
    _ = glfwSetErrorCallback(errorCallback);
    if (glfwInit() == GLFW_FALSE) return error.GlfwFailedToInit;
    defer glfwTerminate();
    
    //glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    
    if (std.builtin.mode == .Debug) {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
    } else {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    window.handle = glfwCreateWindow(1280, 720, "pisc", null, null)
        orelse @panic("Not able to create window.handle");
    defer glfwDestroyWindow(window.handle);
    
    if (glfwRawMouseMotionSupported() != 0) {
        _ = glfwSetInputMode(window.handle, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    }
    
    
    glfwMakeContextCurrent(window.handle);
    
    _ = glfwSetFramebufferSizeCallback(window.handle, framebufferSizeCallback);
    _ = glfwSetScrollCallback(window.handle, mouseScrollCallback);
    
    glfwSetInputMode(window.handle, GLFW_STICKY_KEYS, GLFW_TRUE);
    
    _ = glfwSetMouseButtonCallback(window.handle, mouseButtonCallback);
    _ = glfwSetCharCallback(window.handle, characterCallback);
    
    glfwSwapInterval(1);
    
    // Initialize main context
    window.width = 1280;
    window.height = 720;
    
    glfwSetWindowSizeLimits(window.handle, 800, 600, GLFW_DONT_CARE, GLFW_DONT_CARE);
    
    @import("dlib/gl.zig").loadFunctions(glfwGetProcAddress) catch @panic("Unable to load opengl functions\n");
    
    
    @import(main_file_path).app_main();
}

fn errorCallback(err: c_int, msg: [*c]const u8) callconv(.C) void {
    std.debug.panic("\n GLFW error callback n:{}\n{s}\n", .{ err, msg });
}

fn framebufferSizeCallback(win: ?*GLFWwindow, width: c_int, height: c_int) callconv(.C) void {
    
    window.height = @intCast(u32, height);
    window.width = @intCast(u32, width);
    render.resize_viewport(0, 0, width, height);
}

fn mouseScrollCallback(win: ?*GLFWwindow, x: f64, y: f64) callconv(.C) void {
    input.mouse_scroll = [_]f32{ @floatCast(f32, x), @floatCast(f32, y) };
}

pub var cursor_is_enabled = true;

pub fn disableCursor() void {
    glfwSetInputMode(window.handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    cursor_is_enabled = false;
}

pub fn enableCursor() void {
    glfwSetInputMode(window.handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    cursor_is_enabled = true;
}

// TODO(Samuel): Do this with a non callback function
fn mouseButtonCallback(win: ?*GLFWwindow, button: c_int, action: c_int, mods: c_int) callconv(.C) void {
    switch (action) {
        GLFW_PRESS => {
            var i: u3 = 0;
            while (i < 4) : (i += 1) {
                const bit = @intCast(u8, @as(u8, 1) << i);
                if (button == i) {
                    if ((input.mouse_pressed & bit) != bit) {
                        input.mouse_down = input.mouse_down | bit;
                    }
                    input.mouse_pressed = input.mouse_pressed | bit;
                }
            }
        },
        GLFW_RELEASE => {
            var i: u3 = 0;
            while (i < 4) : (i += 1) {
                const bit = @intCast(u8, @as(u8, 1) << i);
                if (button == i) {
                    if ((input.mouse_pressed & bit) == bit) {
                        input.mouse_up = input.mouse_up | bit;
                    }
                    input.mouse_pressed = input.mouse_pressed & (~bit);
                }
            }
        },
        else => {},
    }
}

fn characterCallback(win: ?*GLFWwindow, codepoint: c_uint) callconv(.C) void {
    if (input.character_input_queue_size < input.character_input_queue.len) {
        input.character_input_queue[input.character_input_queue_size] = codepoint;
        input.character_input_queue_size += 1;
    }
}

// Public functions

pub const poll_events = glfwPollEvents;
pub const get_time = glfwGetTime;

pub fn swapBuffers() void {
    glfwSwapBuffers(window.handle);
}

pub fn mousePos() [2]f32 {
    var xpos: f64 = 0.0;
    var ypos: f64 = 0.0;
    
    glfwGetCursorPos(window.handle, &xpos, &ypos);
    
    const pos = [2]f32 {
        @floatCast(f32, xpos),
        @floatCast(f32, ypos),
    };
    
    return pos;
}

pub fn getKeyState(key: Key) KeyState {
    const pkey = keymap[@enumToInt(key)];
    const state = glfwGetKey(window.handle, pkey);
    return @intToEnum(KeyState, state);
}

pub fn shouldClose() bool {
    return glfwWindowShouldClose(window.handle) != 0;
}

pub fn changeWinSize(width: u32, height: u32) void {
    glfwSetWindowSize(window.handle, @intCast(c_int, width), @intCast(c_int, height));
}

pub fn setCursorPos(x: f64, y: f64) void {
    glfwSetCursorPos(width, x, y);
}

pub var is_fullscreen = false;

/// This function switch a window to fullscren
/// NOTE this function need to be called from the main thread
pub fn switchFullscreen() void {
    const primary_monitor = glfwGetPrimaryMonitor();
    const video_mode = glfwGetVideoMode(primary_monitor);
    
    if (is_fullscreen) {
        is_fullscreen = false;
        glfwSetWindowMonitor(window.handle, null, @divFloor(video_mode.*.width, 2) - 400, @divFloor(video_mode.*.height, 2) - 300, 1280, 720, video_mode.*.refreshRate);
    } else {
        is_fullscreen = true;
        glfwSetWindowMonitor(window.handle, primary_monitor, 0, 0, video_mode.*.width, video_mode.*.height, video_mode.*.refreshRate);
    }
}


pub fn get_pixel_per_milimeter() f32 {
    var x: c_int = 0;
    var y: c_int = 0;
    const monitor = glfwGetPrimaryMonitor() orelse return 0;
    const mode = glfwGetVideoMode(monitor) orelse return 0;
    glfwGetMonitorPhysicalSize(monitor, &x, &y);
    const result = @intToFloat(f32, mode.*.width) / @intToFloat(f32, x);
    return result;
}