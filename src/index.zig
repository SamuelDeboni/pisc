pub usingnamespace @import("dlib/dlib.zig");

pub usingnamespace @import("cpu.zig");
pub usingnamespace @import("editor_draw.zig");
pub usingnamespace @import("assembler.zig");
pub usingnamespace @import("lexer.zig");


pub var monitor_font: render.Font = .{};
pub var monitor_font_2x: render.Font = .{};

pub var render_2x = false;