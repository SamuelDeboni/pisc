usingnamespace @import("index.zig");

const print = std.debug.print;

pub fn app_main() void {
    
    const status = assembly(@embedFile("test_programs/test1.pisc"), plt.g_allocator);
    
    if (!status.ok) {
        std.debug.print("Compilation error msg: {s}\n", .{status.msg});
        return;
    }
    
    const f = std.fs.cwd().createFile("debug.bin", .{ .read = true, .truncate = true }) catch return;
    f.writer().print("{s}", .{status.debug}) catch return;
    
    const in = std.io.getStdIn().reader();
    
    var cpu = CPU{};
    
    const prog = status.output;
    
    for (prog[0..]) |it, i| {
        cpu.mem[i] = it;
    }
    var monitor = create_monitor_from_bytecode(plt.g_allocator, cpu.mem[0 .. 512]);
    
    var buffer = [_]u8{0} ** 512;
    
    render.init();
    ui_camera.transform.position.z = 10;
    
    monitor_font = render.bake_font(plt.g_allocator, 20, 0, 128, @embedFile("../assets/liberation-mono.ttf")) catch {
        @panic("Error in baking font\n");
    };
    
    monitor_font_2x = render.bake_font(plt.g_allocator, 40, 0, 128, @embedFile("../assets/liberation-mono.ttf")) catch {
        @panic("Error in baking font\n");
    };
    
    var exit = false;
    while(!exit) {
        input.poll_input();
        plt.poll_events();
        
        
        if (render_2x) {
            draw_monitor(&monitor, &cpu, 0.016, monitor_font_2x);
            draw_stack_viewer(&cpu, 0.016, monitor_font_2x);
            draw_registers(&cpu, 0.016, monitor_font_2x);
        } else {
            draw_monitor(&monitor, &cpu, 0.016, monitor_font);
            draw_stack_viewer(&cpu, 0.016, monitor_font);
            draw_registers(&cpu, 0.016, monitor_font);
        }
        
        plt.swapBuffers();
        render.clear(0.0, 0.0, 0.0);
        
        if (input.key_down(._1)) {
            render_2x = !render_2x;
        }
        
        
        if (input.key_pressed(.left_alt) and input.key_down(.enter)) {
            plt.switchFullscreen();
        }
        
        if (false) {
            const dpi = plt.get_pixel_per_milimeter();
            if (dpi > 6) {
                render_2x = plt.window.width > 1280;
            } else {
                render_2x = false;
            }
        }
        
        if (!render_2x) {
            ui_camera.size = (@intToFloat(f32, plt.window.width) / 512.0);
            render.font_scale = 2.0 / 480.0;
        } else {
            ui_camera.size = (@intToFloat(f32, plt.window.width) / 1024.0);
            render.font_scale = 2.0 / 960.0;
        }
        
        render.camera = ui_camera;
        
        if (input.key_down(.escape) or plt.shouldClose()) {
            exit = true;
        }
        
        if (input.key_down(.space)) {
            cpu_do_clock(&cpu);
        }
    }
}

pub var ui_camera = render.Camera{ .ortogonal = true, .size = 2.0};