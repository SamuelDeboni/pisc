import sys

if len(sys.argv) != 3:
  print("Expected input file name and output file name")
  exit(1)

f = open(sys.argv[1])
filecontent = f.read()
f.close()

labels = {}
macros = {}
parser_table = []

tokens = filecontent.split()

i = 0
while True:
  if i >= len(tokens): break
  tk = tokens[i]
  if tk == "labels":
    row_number = 0
    while True:
      i += 1
      if i >= len(tokens): break
      tk = tokens[i]
      if tk == "end": break
      labels[tk] = row_number
      row_number += 1
      parser_table.append([255] * 129)
    print(labels)
    print(parser_table)
  elif tk == "macros":
    macro_value = 0
    while True:
      i += 1
      if i >= len(tokens): break
      tk = tokens[i]
      if tk == "end": break
      macros[tk] = macro_value
      macro_value += 1
    print(macros)
  else:
    label1 = tk
    i += 1
    if i >= len(tokens): exit(1)
    symbol = tokens[i]
    i += 1
    if i >= len(tokens): exit(1)
    label2 = tokens[i]

    if symbol[0] == '\'': 
      s = ord(symbol[1])
      parser_table[labels[label1]][s] = labels[label2]
    elif symbol[0] == '\\':
      if symbol[1] == 'w':
        for idx in range(ord('a'), ord('z') + 1):
          parser_table[labels[label1]][idx] = labels[label2]
      elif symbol[1] == 'W':
        for idx in range(ord('A'), ord('Z') + 1):
          parser_table[labels[label1]][idx] = labels[label2]
      elif symbol[1] == 'd':
        for idx in range(ord('0'), ord('9') + 1):
          parser_table[labels[label1]][idx] = labels[label2]
      elif symbol[1] == 'n':
        parser_table[labels[label1]][10] = labels[label2]
      else:
        print("Dont find "+symbol[1]+" char group")
        exit(1)
    elif symbol == "halt":
      value = macros[label2]
      parser_table[labels[label1]][127] = value
    elif symbol == "halt_param":
      value = 1
      if label2 in macros:
        value = macros[label2]
      elif label2 in labels:
        value = labels[label2]
      else:
        print("TODO")
        exit(1)
      parser_table[labels[label1]][128] = value
    else:
      print("Param "+symbol+" not recognized")
      exit(1)
    print(parser_table)
  i += 1

f2 = open(sys.argv[2], "wb")
for row in parser_table:
  for c in row:
    f2.write(c.to_bytes(1, "big"))
f2.close()