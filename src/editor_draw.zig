usingnamespace @import("index.zig");


pub const MonitorLine = struct {
    addr: u16 = 0,
    opcode: u8 = 0,
    arg_count: u16 = 0,
    argument: u16 = 0,
};


pub const Monitor = struct {
    lines: []const MonitorLine = undefined,
    top_line: u32 = 0,
};


var dummy_lines = std.mem.zeroes([512]MonitorLine);

pub fn create_monitor_from_bytecode(al: *Allocator, bytecode: []const u8) Monitor {
    
    var line_count: u32 = 0;
    
    var i: u32 = 0;
    while(i < bytecode.len) : (i += 1) {
        if (bytecode[i] >> 7 == 1) {
            i += 1;
        }
        
        line_count += 1;
    }
    
    var lines = al.alloc(MonitorLine, line_count) catch {
        return Monitor{
            .lines = dummy_lines[0 ..],
        };
    };
    
    i = 0;
    var li: u32 = 0;
    while(i < bytecode.len) : (i += 1) {
        lines[li] = .{
            .addr = @intCast(u16, i),
            .opcode = bytecode[i],
        };
        
        if (bytecode[i] >> 7 == 1) {
            i += 1;
            lines[li].argument = bytecode[i];
            lines[li].arg_count = 1;
        }
        
        li += 1;
    }
    
    return Monitor {
        .lines = lines,
    };
}


const mn_list0 = [_][]const u8 {
    "push",
    "pushi",
    "pop",
    "jne",
    "jeq",
};


const mn_list1 = [_][]const u8 {
    "nop",
    "add",
    "sub",
    "mul",
    "div",
    "or",
    "and",
    "xor",
    "not",
    "sl",
    "sra",
    "srl",
    "cmp",
    "jmp",
    "load",
    "store",
    "...",
    "inc",
    "dec",
};

fn opcode_to_mnemonic(opcode: u8) []const u8 {
    
    if (opcode >= 0xf0) {
        const op_i = opcode - 0xf0;
        if (op_i >= mn_list0.len) return "Err";
        return mn_list0[op_i];
    } else {
        if (opcode >= mn_list1.len) return "Err";
        return mn_list1[opcode];
    }
}

fn get_label_from_addr(addr: u16) ?[]const u8 {
    if (addr == 0) return "START:";
    //if (addr == 18) return "@push 2561"; 
    return null;
}

const font_color_0 = Color.c(1.0, 1.0, 1.0, 1.0);
const font_color_1 = Color.c(0.5, 1.0, 0.5, 1.0);
const font_color_2 = Color.c(1.0, 0.5, 0.25, 1.0);
const font_color_3 = Color.c(0.5, 0.5, 1.0, 1.0);
const font_color_4 = Color.c(1.0, 1.0, 0.5, 1.0);

var buff = [_]u8{0} ** 512;
pub fn draw_monitor(monitor: *Monitor, cpu: *CPU, delta: f32, font: render.Font) void {
    
    var font_color = font_color_0;
    
    const cam_size = render.camera_size();
    
    var font_y: f32 =  cam_size.y - 0.2;
    var font_x: f32 = -cam_size.x + 0.1;
    
    render.draw_solid_rect(Rect.c(font_x - 0.05, -font_y - 0.125, 1.25, cam_size.y * 2 - 0.175), Color.c(0.1, 0.1, 0.1, 1.0), 0.0);
    
    const y_separation: f32 = 0.075;
    const line_count = @floatToInt(u32, (cam_size.y * 2 - 0.2) / y_separation);
    
    var i: u32 = 0;
    while (i < line_count) : (i += 1) {
        if (i >= monitor.lines.len or font_y < -cam_size.y + 0.075) break;
        
        if (monitor.lines[i].addr == cpu.pc) {
            font_color = font_color_2;
        } else {
            font_color = font_color_0;
        }
        
        const line = monitor.lines[i];
        
        // NOTE(samuel): Draw Labels and pseudoinstruction
        if (get_label_from_addr(line.addr)) |label| {
            render.draw_text(label, font_x, font_y, font, font_color_1, 0);
            font_y -= y_separation;
        }
        if (font_y < -cam_size.y + 0.075) break;
        
        // NOTE(samuel): Draw address
        {
            const addr_t = std.fmt.bufPrint(&buff, "{x:4}h", .{line.addr}) catch "Err";
            render.draw_text(addr_t, font_x, font_y, font, font_color, 0);
        }
        
        // NOTE(samuel): Draw Opcodes
        {
            const opcode_t = std.fmt.bufPrint(&buff, "0x{x}", .{line.opcode}) catch "Err";
            render.draw_text(opcode_t, font_x + 0.3, font_y, font, font_color, 0);
            
            if (line.arg_count > 0) {
                const arg_t = std.fmt.bufPrint(&buff, "0x{x}", .{line.argument}) catch "Err";
                render.draw_text(arg_t, font_x + 0.5, font_y, font, font_color, 0);
            }
        }
        
        // NOTE(samuel): Draw Mnemonic
        {
            render.draw_text(opcode_to_mnemonic(line.opcode), font_x + 0.75, font_y, font, font_color, 0);
            
            if (line.arg_count > 0) {
                const arg_t = std.fmt.bufPrint(&buff, "{}", .{line.argument}) catch "Err";
                render.draw_text(arg_t, font_x + 1, font_y, font, font_color, 0);
            }
            
        }
        
        font_y -= y_separation;
    }
}


pub fn draw_stack_viewer(cpu: *CPU, delta: f32, font: render.Font) void {
    
    var font_color = font_color_0;
    
    const cam_size = render.camera_size();
    
    var font_y: f32 =  cam_size.y - 0.2;
    var font_x: f32 = -cam_size.x + 1.4;
    
    render.draw_solid_rect(Rect.c(font_x - 0.05, -font_y - 0.125, 0.75, cam_size.y * 2 - 0.175), Color.c(0.1, 0.1, 0.1, 1.0), 0.0);
    
    const y_separation: f32 = 0.075;
    const line_count = @floatToInt(u32, (cam_size.y * 2 - 0.2) / y_separation);
    
    var i: u32 = 0;
    while (i < line_count) : (i += 1) {
        const si = cpu.sp + @intCast(u16, i) * 2;
        
        var s = std.fmt.bufPrint(&buff, "{x:4}h", .{si}) catch "Err";
        render.draw_text(s, font_x, font_y, font, font_color, 0);
        
        const stack_value = cpu_mem_load(cpu, si);
        
        s = std.fmt.bufPrint(&buff, "{d:6}", .{stack_value}) catch "Err";
        render.draw_text(s, font_x + 0.4, font_y, font, font_color, 0);
        
        font_y -= y_separation;
    }
}

pub fn draw_registers(cpu: *CPU, delta: f32, font: render.Font) void {
    
    var font_color = font_color_0;
    
    const cam_size = render.camera_size();
    
    var font_y: f32 =  cam_size.y - 0.2;
    const start_font_x: f32 = -cam_size.x + 2.25;
    var font_x: f32 = start_font_x;
    
    const width = (cam_size.x - 0.1) - font_x;
    var row_count = @floatToInt(i32, width * 2); 
    if (row_count <= 0) row_count = 1;
    
    const line_count = @divFloor(8, row_count) + 3;
    
    const y_separation: f32 = 0.075;
    const height = @intToFloat(f32, line_count) * y_separation + 0.15;
    
    {
        const rect = Rect {
            .x = font_x - 0.05,
            .y = font_y - height + 0.1,
            .w = width,
            .h = height,
        };
        render.draw_solid_rect(rect, Color.c(0.1, 0.1, 0.1, 1.0), 0.0);
    }
    
    //~ NOTE(samuel): Draw SP 
    {
        render.draw_text("PC =", font_x, font_y, font, font_color_0, 0);
        var s = std.fmt.bufPrint(&buff, "0x{x}", .{cpu.pc}) catch "Err";
        render.draw_text(s, font_x + 0.22, font_y, font, font_color_4, 0);
        font_y -= y_separation;
    }
    
    //~ NOTE(samuel): Draw PC 
    {
        render.draw_text("SP =", font_x, font_y, font, font_color_0, 0);
        var s = std.fmt.bufPrint(&buff, "0x{x}", .{cpu.sp}) catch "Err";
        render.draw_text(s, font_x + 0.22, font_y, font, font_color_4, 0);
        font_y -= y_separation;
    }
    font_y -= y_separation;
    
    var i: u32 = 0;
    while (i < cpu.r.len) : (i += 1) {
        const r = cpu.r[i];
        
        var s = std.fmt.bufPrint(&buff, "r{} =", .{i}) catch "Err";
        render.draw_text(s, font_x, font_y, font, font_color_0, 0);
        s = std.fmt.bufPrint(&buff, "0x{x}", .{r}) catch "Err";
        render.draw_text(s, font_x + 0.22, font_y, font, font_color_4, 0);
        
        font_x += 0.5;
        
        if (i % @intCast(u32, row_count) == @intCast(u32, row_count - 1)) {
            font_y -= y_separation;
            font_x = start_font_x;
        }
    }
}

