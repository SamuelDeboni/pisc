const std = @import("std");
const print = std.debug.print;

pub var enable_lexer_verbose = true;

const LexerHaltType = enum(u8) {
    success,
    catch_any,
    err,
};

pub const TokenType = enum(u8) {
    new_line,
    mnemonic,
    integer,
    string,
    label,
    label_decl,
    pseudo,
    constant,
    equals,
};

pub const LexerError = error {
    EndOfSource,
    ErrorHaltCode,
    InvalidHaltCode,
};

pub const TokenPair = struct {
    token: []const u8,
    meaning: TokenType,
};

pub const LexerContext = struct {
    table: []const u8,
    source: []const u8,
    pointer: usize = 0,
    line_count: usize = 1,
    relative_char_count: usize = 0,
    max_token_size: usize = 200,
    comment_symbol: u8 = '#',
    last_pair: TokenPair = undefined,
    last_token_position: usize = 0,
};

pub fn get_token(ctx: *LexerContext) LexerError!TokenPair {
    // jump white spaces
    while (ctx.pointer < ctx.source.len and (ctx.source[ctx.pointer] == ' '
    or ctx.source[ctx.pointer] == '\t'
    or ctx.source[ctx.pointer] == '\r')) {
        ctx.pointer += 1;
    }

    // jump comments
    while (ctx.pointer < ctx.source.len and ctx.source[ctx.pointer] == ctx.comment_symbol) {
        while (ctx.pointer < ctx.source.len and ctx.source[ctx.pointer] != '\n') {
            ctx.pointer += 1;
        }
        if (ctx.pointer < ctx.source.len and ctx.source[ctx.pointer] == '\n') {
            ctx.pointer += 1;
        }
        while (ctx.pointer < ctx.source.len and (ctx.source[ctx.pointer] == ' ' 
        or ctx.source[ctx.pointer] == '\t'
        or ctx.source[ctx.pointer] == '\r')) {
            ctx.pointer += 1;
        }
    }

    // navegate into parse table for find the next token and this meaning
    var pair: TokenPair = undefined;
    pair.token.ptr = ctx.source[ctx.pointer..].ptr;
    pair.token.len = 0;

    ctx.last_token_position = ctx.pointer;

    var row: u8 = 0;
    while (true) {
        if (ctx.pointer < ctx.source.len and pair.token.len < ctx.max_token_size) {
            const it = ctx.source[ctx.pointer];
            if (it >= 0 and it <= 127 and ctx.table[@intCast(usize, row)*129 + it] != 255) {
                pair.token.len += 1;
                ctx.pointer += 1;
                row = ctx.table[@intCast(usize, row)*129 + it];
            } else {
                const halt = ctx.table[@intCast(usize, row)*129 + 127];
                switch (halt) {
                    @enumToInt(LexerHaltType.success) => {
                        pair.meaning = @intToEnum(TokenType, ctx.table[@intCast(usize, row)*129 + 128]);
                        break;
                    },
                    @enumToInt(LexerHaltType.catch_any) => {
                        pair.token.len += 1;
                        ctx.pointer += 1;
                        row = ctx.table[@intCast(usize, row)*129 + 128];
                    },
                    @enumToInt(LexerHaltType.err) => {
                        print("Received error halt. row: {} char: {c}", .{row, it});
                        return error.ErrorHaltCode;
                    },
                    else => {
                        print("Invalid halt code: {} row: {}", .{halt, row});
                        return error.InvalidHaltCode;
                    },
                }
            }
        } else {
            if (pair.token.len == 0) {
                return error.EndOfSource;
            }

            const halt = ctx.table[@intCast(usize, row)*129 + 127];
            switch (halt) {
                @enumToInt(LexerHaltType.success) => {
                    pair.meaning = @intToEnum(TokenType, ctx.table[@intCast(usize, row)*129 + 128]);
                    break;
                },
                @enumToInt(LexerHaltType.err) => {
                    print("Received error halt. row: {}", .{row});
                    return error.ErrorHaltCode;
                },
                else => {
                    print("Invalid halt code: {} row: {}", .{halt, row});
                    return error.InvalidHaltCode;
                },
            }
        }
    }

    if (enable_lexer_verbose) {
        if (pair.meaning != .new_line) {
            print("Token: {s} Meaning: {}\n", .{pair.token, pair.meaning});
        } else {
            print("Token: \\n Meaning: {}\n", .{pair.meaning});
        }
    }

    ctx.last_pair = pair;

    return pair;
}