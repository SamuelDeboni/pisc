usingnamespace @import("index.zig");

pub const CPU = struct {
    pub const memory_size = 8196;
    
    stack_start: u16 = 4096,
    sp:   u16 = 4096,
    pc:   u16 = 0,
    mem:  [memory_size]u8 = [_]u8{0} ** memory_size,
    r: [8]u16 = [_]u16{0} ** 8,
};


pub const ops = struct {
    pub const push:  u8 = 0xf0;
    pub const pushi: u8 = 0xf1;
    pub const pop:   u8 = 0xf2;
    pub const jne:   u8 = 0xf3;
    pub const jeq:   u8 = 0xf4;
    
    pub const nop:   u8 = 0x00;
    pub const add:   u8 = 0x01;
    pub const sub:   u8 = 0x02;
    pub const mul:   u8 = 0x03;
    pub const div:   u8 = 0x04;
    pub const _or:   u8 = 0x05;
    pub const _and:  u8 = 0x06;
    pub const xor:   u8 = 0x07;
    pub const _not:  u8 = 0x08;
    pub const sl:    u8 = 0x09;
    pub const sra:   u8 = 0x0a;
    pub const srl:   u8 = 0x0b;
    pub const cmp:   u8 = 0x0c;
    pub const jmp:   u8 = 0x0d;
    pub const load:  u8 = 0x0e;
    pub const store: u8 = 0x0f;
    pub const inc:   u8 = 0x11;
    pub const dec:   u8 = 0x12;
};

pub fn cpu_mem_load(cpu: *CPU, addr: u16) u16 {
    if (addr >= CPU.memory_size - 1) return 0;
    
    const vl: u16 = cpu.mem[addr];
    const vr: u16 = cpu.mem[addr + 1];
    const result = vl + (vr << 8);
    return result;
}

pub fn cpu_mem_store(cpu: *CPU, addr: u16, value: u16) void {
    if (addr >= CPU.memory_size - 1) return;
    
    cpu.mem[addr]     = @intCast(u8, value & 0xff);
    cpu.mem[addr + 1] = @intCast(u8, value >> 8);
}

pub fn cpu_push(cpu: *CPU, value: u16) void {
    cpu.sp -%= 2;
    
    cpu.mem[cpu.sp]     = @intCast(u8, value & 0xff);
    cpu.mem[cpu.sp + 1] = @intCast(u8, value >> 8);
}

pub fn cpu_pop(cpu: *CPU) u16 {
    const vl: u16 = cpu.mem[cpu.sp];
    const vr: u16 = cpu.mem[cpu.sp + 1];
    cpu.sp +%= 2;
    
    const result = vl + (vr << 8);
    return result;
}

pub fn cpu_do_clock(cpu: *CPU) void {
    
    const op = cpu.mem[cpu.pc];
    cpu.pc +%= 1;
    
    switch(op) {
        ops.push => {
            const i: u16 = cpu.mem[cpu.pc];
            cpu.pc +%= 1;
            
            const v = cpu.r[i];
            cpu_push(cpu, v);
        },
        
        ops.pushi => {
            const i: u16 = cpu.mem[cpu.pc];
            cpu.pc +%= 1;
            
            cpu_push(cpu, i);
        },
        
        ops.pop => {
            const i: u16 = cpu.mem[cpu.pc];
            cpu.pc +%= 1;
            
            const v  = cpu_pop(cpu);
            cpu.r[i] = v;
        },
        
        ops.jne => {
            const i: u16 = cpu.mem[cpu.pc];
            cpu.pc +%= 1;
            
            const v    = cpu_pop(cpu);
            const addr = cpu_pop(cpu);
            
            if (i != v) {
                cpu.pc = addr;
            }
        },
        
        ops.jeq => {
            const i: u16 = cpu.mem[cpu.pc];
            cpu.pc +%= 1;
            
            const v    = cpu_pop(cpu);
            const addr = cpu_pop(cpu);
            
            if (i == v) {
                cpu.pc = addr;
            }
        },
        
        //------------------------------------
        ops.nop => {},
        
        ops.add => {
            const a = cpu_pop(cpu);
            const b = cpu_pop(cpu);
            const c = a +% b;
            cpu_push(cpu, c);
        },
        
        ops.sub => {
            const a = cpu_pop(cpu);
            const b = cpu_pop(cpu);
            const c = a -% b;
            cpu_push(cpu, c);
        },
        
        ops.mul => {
            const a = cpu_pop(cpu);
            const b = cpu_pop(cpu);
            const c = a *% b;
            cpu_push(cpu, c);
        },
        
        ops.div => {
            const a = cpu_pop(cpu);
            const b = cpu_pop(cpu);
            const c = a / b;
            cpu_push(cpu, c);
        },
        
        ops._or => {
            const a = cpu_pop(cpu);
            const b = cpu_pop(cpu);
            const c = a | b;
            cpu_push(cpu, c);
        },
        
        ops._and => {
            const a = cpu_pop(cpu);
            const b = cpu_pop(cpu);
            const c = a & b;
            cpu_push(cpu, c);
        },
        
        ops.xor => {
            const a = cpu_pop(cpu);
            const b = cpu_pop(cpu);
            const c = a ^ b;
            cpu_push(cpu, c);
        },
        
        ops._not => {
            const a = cpu_pop(cpu);
            const b = ~a;
            cpu_push(cpu, b);
        },
        
        ops.sl => {
            const a = cpu_pop(cpu);
            const b = @intCast(u4, cpu_pop(cpu) & 0xff);
            const c = a << b;
            cpu_push(cpu, c);
        },
        
        ops.sra => {
            const a = cpu_pop(cpu);
            const b = @intCast(u4, cpu_pop(cpu) & 0xff);
            const c = a >> b;
            cpu_push(cpu, c);
        },
        
        ops.srl => {
            const a = cpu_pop(cpu);
            const b = @intCast(u4, cpu_pop(cpu) & 0xff);
            const c = a >> b;
            cpu_push(cpu, c);
        },
        
        ops.cmp => {
            const a = cpu_pop(cpu);
            const b = cpu_pop(cpu);
            
            if (a > b) {
                cpu_push(cpu, 1);
            } else if (a < b) {
                cpu_push(cpu, @bitCast(u16, @as(i16, -1)));
            } else {
                cpu_push(cpu, 0);
            }
        },
        
        ops.jmp => {
            const addr = cpu_pop(cpu);
            cpu.pc = addr;
        },
        
        ops.load => {
            const addr = cpu_pop(cpu);
            const value = cpu_mem_load(cpu, addr);
            cpu_push(cpu, value);
            cpu_push(cpu, addr +% 2);
        },
        
        ops.store => {
            const addr = cpu_pop(cpu);
            const value = cpu_pop(cpu);
            cpu_mem_store(cpu, addr, value);
            cpu_push(cpu, addr +% 2);
        },
        
        ops.inc => {
            const v = cpu_pop(cpu);
            cpu_push(cpu, v +% 1);
        },
        
        ops.dec => {
            const v = cpu_pop(cpu);
            cpu_push(cpu, v -% 1);
        },
        
        else => {},
    }
}
