usingnamespace @import("duilib_def.zig");
const std = @import("std");
const assert = std.debug.assert;

pub const UiColor = struct {
    r: f32 = 0.0,
    g: f32 = 0.0,
    b: f32 = 0.0,
    a: f32 = 1.0,
    
    pub fn c(r: f32, g: f32, b: f32, a: f32) UiColor {
        return UiColor {
            .r = r, .g = g, .b = b, .a = a,
        };
    }
};

pub const UiRect = struct {
    x: f32 = 0.0,
    y: f32 = 0.0,
    w: f32 = 1.0,
    h: f32 = 1.0,
    
    pub fn c(x: f32, y: f32, w: f32, h: f32) UiRect {
        return UiRect {
            .x = x, .y = y, .w = w, .h = h,
        };
    }
};

const WidgetType = enum {
    none,
    button,
    button_toggle,
    slider,
    check_box,
    movable,
};

const UiId = struct {
    widget_type: WidgetType = .none,
    label: []const u8 = "",
};


pub const UiContext = struct {
    active_id: UiId = .{},
    active_label_buffer: [256]u8 = [_]u8{0} ** 256,
    
    hot_id: UiId = .{},
    hot_label_buffer: [256]u8 = [_]u8{0} ** 256,
    
    mouse_pos: [2]f32 = [2]f32 {0.0, 0.0},
    mouse_up: bool = false,
    mouse_down: bool = false,
    
    width: f32 = 0.0,
    height: f32 = 0.0,
    
    font: Font = undefined,
    font_h: f32 = 0.0,
    
    al: std.heap.FixedBufferAllocator = undefined,
};

fn is_active(ctx: *UiContext, id: UiId) bool {
    const result = ctx.active_id.widget_type == id.widget_type and
        std.mem.eql(u8, ctx.active_id.label, id.label);
    
    return result;
}

fn is_hot(ctx: *UiContext, id: UiId) bool {
    const result = ctx.hot_id.widget_type == id.widget_type and
        std.mem.eql(u8, ctx.hot_id.label, id.label);
    
    return result;
}

fn set_active(ctx: *UiContext, id: UiId) void {
    ctx.active_id.widget_type = id.widget_type;
    assert(id.label.len < 256);
    
    const l = ctx.active_label_buffer[0 .. id.label.len];
    for (l) |*it, i| it.* = id.label[i];
    ctx.active_id.label = l;
}

fn set_hot(ctx: *UiContext, id: UiId) void {
    ctx.hot_id.widget_type = id.widget_type;
    assert(id.label.len < 256);
    
    const l = ctx.hot_label_buffer[0 .. id.label.len];
    for (l) |*it, i| it.* = id.label[i];
    ctx.hot_id.label = l;
}


pub const LayoutTag = enum {
    list,
    grid,
};

pub const Layout = struct {
    ctx: *UiContext = undefined,
    x: f32 = 0,
    y: f32 = 0,
    w: f32 = 0,
    h: f32 = 0,
    max_x: f32 = 0,
    max_w: f32 = 0,
    spacing: f32 = 0,
    
    l_union: union (LayoutTag) {
        list: struct {},
        grid: struct {
            col_number: u32 = 0,
            col_count: u32 = 0,
        },
    } = .{ .list = .{}},
};

pub fn listLayout(ctx: *UiContext, x: f32, y: f32, w: f32, h: f32, spacing: f32) Layout {
    const result = Layout {
        .ctx = ctx,
        .x = x,
        .max_x = x,
        .y = y,
        .w = w,
        .max_w = w,
        .h = h,
        .spacing = spacing,
    };
    
    return result;
}

pub fn gridLayout(ctx: *UiContext, x: f32, y: f32, w: f32, h: f32, spacing: f32, col_number: u32) Layout {
    const result = Layout {
        .ctx = ctx,
        .x = x,
        .y = y,
        .w = w,
        .h = h,
        .max_x = x,
        .max_w = w,
        .spacing = spacing,
        
        .l_union = .{
            .grid = .{
                .col_number = col_number,
            }
        },
    };
    
    return result;
}

fn doLayout(layout: *Layout) void {
    switch (layout.l_union) {
        .list => {
            layout.y -= layout.h + layout.spacing;
            layout.max_x = if (layout.x > layout.max_x) layout.x else layout.max_x;
        },
        
        .grid => |*lu| {
            lu.col_count += 1;
            if (lu.col_count < lu.col_number) {
                layout.x += layout.w + layout.spacing;
            } else {
                lu.col_count = 0;
                layout.y -= layout.h + layout.spacing;
                layout.x -= (layout.w + layout.spacing) *
                    @intToFloat(f32, lu.col_number - 1);
                
                layout.max_x = if (layout.x > layout.max_x) layout.x else layout.max_x;
            }
        },
    }
}

pub const PanelInfo = struct {
    x: f32 = 0.0,
    y: f32 = 0.0,
    layout: Layout = undefined,
    
};


pub fn lStartPanel(ctx: *UiContext, label: []const u8, x: f32, y: f32, w: f32, h: f32, spacing: f32) PanelInfo {
    
    var _x = x;
    var _y = y;
    _ = movable(ctx, label, &_x, &_y, w, h);
    
    var layout = listLayout(ctx, _x, _y, w, h, spacing);
    doLayout(&layout);
    layout.y -= h * 0.5;
    
    const result = PanelInfo {
        .x = _x,
        .y = _y,
        .layout = layout,
    };
    
    return result;
}

pub fn lEndPanel(info: PanelInfo) void {
    const l = info.layout;
    const w = (l.max_x - l.x) + l.max_w + 0.1 * l.w;
    const h = info.y - l.y + l.h * 0.5;
    box(info.x - l.w * 0.05, l.y + l.h * 0.5, w, h, 0.99);
}

pub fn lButton(layout: *Layout, label: []const u8) bool {
    var result = false;
    result = button(layout.ctx, label, layout.x, layout.y, layout.w, layout.h);
    doLayout(layout);
    return result;
}

pub fn lButtonToggle(layout: *Layout, label: []const u8, selected: *bool) bool {
    var result = false;
    result = buttonToggle(layout.ctx, label, layout.x, layout.y, layout.w, layout.h, selected);
    doLayout(layout);
    return result;
}

pub fn lCheckBox(layout: *Layout, label: []const u8, selected: *bool) bool {
    var result = false;
    result = checkBox(layout.ctx, label, layout.x, layout.y, layout.h, selected);
    doLayout(layout);
    return result;
}


pub fn lSliderF32(layout: *Layout, label: []const u8, value: *f32) void {
    slider(layout.ctx, label, layout.x, layout.y, layout.w, layout.h, value);
    
    var buf = [_]u8{0} ** 64;
    const value_t = std.fmt.bufPrint(&buf, "{d:0.2}", .{value.*}) catch "err";
    text(layout.ctx, value_t, layout.x + layout.w * 0.8, layout.y + 0.5 * layout.h, layout.w, layout.h * 0.5);
    
    doLayout(layout);
}

pub fn lSliderI32(layout: *Layout, label: []const u8, value: *i32, min: i32, max: i32) void {
    
    const len = @intToFloat(f32, max - min);
    var f_value = @intToFloat(f32, value.* - min) / len;
    slider(layout.ctx, label, layout.x, layout.y, layout.w, layout.h, &f_value);
    value.* = @floatToInt(i32, f_value * len) + min;
    
    var buf = [_]u8{0} ** 64;
    const value_t = std.fmt.bufPrint(&buf, "{d}", .{value.*}) catch "err";
    text(layout.ctx, value_t, layout.x + layout.w * 0.8, layout.y + 0.5 * layout.h, layout.w, layout.h * 0.5);
    
    doLayout(layout);
}

pub fn lSliderF32_range(layout: *Layout, label: []const u8, value: *f32, min: f32, max: f32) void {
    
    const len = max - min;
    var f_value = (value.* - min) / len;
    slider(layout.ctx, label, layout.x, layout.y, layout.w, layout.h, &f_value);
    value.* = f_value * len + min;
    
    var buf = [_]u8{0} ** 64;
    const value_t = std.fmt.bufPrint(&buf, "{d:0.2}", .{value.*}) catch "err";
    text(layout.ctx, value_t, layout.x + layout.w * 0.8, layout.y + 0.5 * layout.h, layout.w, layout.h * 0.5);
    
    doLayout(layout);
}


pub fn lText(layout: *Layout, label: []const u8) void {
    text(layout.ctx, label, layout.x, layout.y, layout.w, layout.h);
    doLayout(layout);
}

pub fn startSubList(layout: *Layout, x_offset: f32) Layout {
    assert(layout.l_union == .list);
    
    const x = layout.x + x_offset * layout.w;
    const result = Layout {
        .ctx = layout.ctx,
        .x = x,
        .y = layout.y,
        .w = layout.w,
        .h = layout.h,
        .max_x = if (x > layout.max_x) x else layout.max_x,
        .max_w = layout.max_w,
        .spacing = layout.spacing,
    };
    
    return result;
}

pub fn startSubGrid(layout: *Layout, w: f32, col_number: u32, x_offset: f32) Layout {
    assert(layout.l_union == .list);
    
    const _w = w / @intToFloat(f32, col_number);
    const x = layout.x + x_offset * _w;
    
    const result = Layout {
        .ctx = layout.ctx,
        .x = x,
        .y = layout.y,
        .w = _w,
        .h = layout.h,
        
        .max_x = if (x > layout.max_x) x else layout.max_x,
        .max_w = if (w > layout.max_w) w else layout.max_w,
        .spacing = layout.spacing,
        
        .l_union = .{
            .grid  = .{
                .col_number = col_number,
            },
        },
    };
    
    return result;
}

pub fn endSubLayout(layout: *Layout, sub_layout: Layout) void {
    assert(layout.l_union == .list);
    
    if (sub_layout.l_union == .list) {
        layout.y = sub_layout.y;
        
        if (layout.max_x < sub_layout.max_x)
            layout.max_x = sub_layout.max_x;
        
        if (layout.max_w < sub_layout.max_w)
            layout.max_w = sub_layout.max_w;
        
    } else if (sub_layout.l_union == .grid) {
        
        if (sub_layout.l_union.grid.col_count == 0) {
            layout.y = sub_layout.y;
        } else {
            layout.y = sub_layout.y - sub_layout.h - sub_layout.spacing;
        }
        
        const smx = sub_layout.max_x + @intToFloat(f32, sub_layout.l_union.grid.col_number) * (sub_layout.spacing);
        
        if (layout.max_x < smx)
            layout.max_x = smx;
        
        if (layout.max_w < sub_layout.max_w)
            layout.max_w = sub_layout.max_w;
    }
}

fn mouseIsInside(ctx: *UiContext, x: f32, y: f32, w: f32, h: f32) bool {
    const mp = ctx.mouse_pos;
    const result = mp[0] >= x and mp[1] >= y and mp[0] <= (x + w) and mp[1] <= (y + h);
    return result;
}


const menu_color = UiColor.c(0.4, 0.4, 0.4, 1.0);
const menu_color_bg =  UiColor.c(0.3, 0.3, 0.3, 1.0);

const box_color = UiColor.c(0.2, 0.2, 0.2, 1.0);
const box_color_bg =  UiColor.c(0.1, 0.1, 0.1, 1.0);

const active_color = UiColor.c(0.8, 0.8, 0.8, 1.0);
const active_color_bg = UiColor.c(0.7, 0.7, 0.7, 1.0);

const hot_color = UiColor.c(0.4, 0.4, 0.4, 1.0);
const hot_color_bg = UiColor.c(0.5, 0.5, 0.5, 1.0);

pub fn movable(ctx: *UiContext, label: []const u8, x: *f32, y: *f32, w: f32, h: f32) bool {
    const id = UiId {
        .widget_type = .movable,
        .label = label,
    };
    
    var result = false;
    
    if (is_active(ctx, id)) {
        if (ctx.mouse_up) {
            if (is_hot(ctx, id)) result = true;
            ctx.active_id = .{};
        }
        
    } if (ctx.mouse_down and is_hot(ctx, id)) {
        set_active(ctx, id);
    }
    
    if (mouseIsInside(ctx, x.*, y.*, w, h)) {
        if (ctx.hot_id.widget_type == .none)
            set_hot(ctx, id);
    } else if (is_hot(ctx, id)) {
        ctx.hot_id = .{};
    }
    
    var color0 = menu_color;
    var color1 = menu_color_bg;
    color0.r = 0.6;
    color1.r = 0.4;
    
    if (is_active(ctx, id)) {
        color1 = active_color;
        color0 = active_color_bg;
        
        color0.r = 1.0;
        color1.r = 1.0;
        
        x.* = ctx.mouse_pos[0] - w * 0.5;
        y.* = ctx.mouse_pos[1] - h * 0.5;
    } else if (is_hot(ctx, id)) {
        color1 = hot_color;
        color0 = hot_color_bg;
        
        color0.r = 0.8;
        color1.r = 0.6;
    }
    
    rectWithBorder(x.*, y.*, w, h, color0, color1);
    text(ctx, label, x.*, y.*, w, h);
    
    return result;
}

pub fn button(ctx: *UiContext, label: []const u8, x: f32, y: f32, w: f32, h: f32) bool {
    const id = UiId {
        .widget_type = .button,
        .label = label,
    };
    
    var result = false;
    
    if (is_active(ctx, id)) {
        if (ctx.mouse_up) {
            if (is_hot(ctx, id)) result = true;
            ctx.active_id = .{};
        }
    } if (ctx.mouse_down and is_hot(ctx, id)) {
        set_active(ctx, id);
    }
    
    if (mouseIsInside(ctx, x, y, w, h)) {
        if (ctx.hot_id.widget_type == .none)
            set_hot(ctx, id);
    } else if (is_hot(ctx, id)) {
        ctx.hot_id = .{};
    }
    
    var color0 = menu_color;
    var color1 = menu_color_bg;
    
    if (is_active(ctx, id)) {
        color1 = active_color;
        color0 = active_color_bg;
    } else if (is_hot(ctx, id)) {
        color1 = hot_color;
        color0 = hot_color_bg;
    }
    
    rectWithBorder(x, y, w, h, color0, color1);
    text(ctx, label, x, y, w, h);
    
    return result;
}

pub fn slider(ctx: *UiContext, label: []const u8, x: f32, y: f32, w: f32, h: f32, value: *f32) void {
    const id = UiId {
        .widget_type = .slider,
        .label = label,
    };
    
    if (is_active(ctx, id)) {
        const mpx = ctx.mouse_pos[0];
        value.* = std.math.clamp((mpx - x) / w, 0.0, 1.0);
        if (ctx.mouse_up) {
            if (is_hot(ctx, id)) {
                
            }
            ctx.active_id = .{};
        }
    } if (ctx.mouse_down and is_hot(ctx, id)) {
        set_active(ctx, id);
    }
    
    const s = h * 0.5;
    const slider_x = x + w * value.* - s * 0.5;
    
    if (mouseIsInside(ctx, slider_x, y, s, s)) {
        if (ctx.hot_id.widget_type == .none) set_hot(ctx, id);
    } else if (is_hot(ctx, id)) {
        ctx.hot_id = .{};
    }
    
    var color0 = menu_color;
    var color1 = menu_color_bg;
    
    if (is_active(ctx, id)) {
        color1 = active_color;
        color0 = active_color_bg;
    } else if (is_hot(ctx, id)) {
        color1 = hot_color;
        color0 = hot_color_bg;
    }
    
    draw_rect(UiRect.c(x, y + s * 0.25, w, s * 0.5), color0);
    rectWithBorder(slider_x, y, s, s, color0, color1);
    
    text(ctx, label, x, y + 0.5 * h, w, h * 0.5);
}

pub fn buttonToggle(ctx: *UiContext, label: []const u8, x: f32, y: f32, w: f32, h: f32, selected: *bool) bool {
    const id = UiId {
        .widget_type = .button_toggle,
        .label = label,
    };
    
    if (is_active(ctx, id)) {
        if (ctx.mouse_up) {
            if (is_hot(ctx, id)) selected.* = !selected.*;
            ctx.active_id = .{};
        }
    } if (ctx.mouse_down and is_hot(ctx, id)) {
        set_active(ctx, id);
    }
    
    if (mouseIsInside(ctx, x, y, w, h)) {
        if (ctx.hot_id.widget_type == .none) set_hot(ctx, id);
    } else if (is_hot(ctx, id)) {
        ctx.hot_id = .{};
    }
    
    var color0 = menu_color;
    var color1 = menu_color_bg;
    
    if (is_active(ctx, id)) {
        color1 = active_color;
        color0 = active_color_bg;
    } else if (is_hot(ctx, id)) {
        color1 = hot_color;
        color0 = hot_color_bg;
    }
    
    if (selected.*) {
        color0.b = 1.0;
        color1.b = 1.0;
    }
    
    rectWithBorder(x, y, w, h, color0, color1);
    text(ctx, label, x, y, w, h);
    
    return selected.*;
}

pub fn checkBox(ctx: *UiContext, label: []const u8, x: f32, y: f32, size: f32, selected: *bool) bool {
    const id = UiId {
        .widget_type = .check_box,
        .label = label,
    };
    
    if (is_active(ctx, id)) {
        if (ctx.mouse_up) {
            if (is_hot(ctx, id)) selected.* = !selected.*;
            ctx.active_id = .{};
        }
    } if (ctx.mouse_down and is_hot(ctx, id)) {
        set_active(ctx, id);
    }
    
    if (mouseIsInside(ctx, x, y, size, size)) {
        if (ctx.hot_id.widget_type == .none) set_hot(ctx, id);
    } else if (is_hot(ctx, id)) {
        ctx.hot_id = .{};
    }
    
    var color0 = menu_color;
    var color1 = menu_color_bg;
    
    if (is_active(ctx, id)) {
        color1 = active_color;
        color0 = active_color_bg;
    } else if (is_hot(ctx, id)) {
        color1 = hot_color;
        color0 = hot_color_bg;
    }
    
    if (selected.*) {
        color0.b = 1.0;
        color1.b = 1.0;
    }
    
    rectWithBorder(x, y, size, size, color0, color1);
    text(ctx, label, x + size * 0.85, y, size, size);
    
    return selected.*;
}

pub fn rectWithBorder(x: f32, y: f32, w: f32, h: f32, color0: UiColor, color1: UiColor) void {
    const dec = h * 0.2;
    const inc = h * 0.1;
    draw_rect(UiRect.c(x, y, w, h), color1);
    draw_rect(UiRect.c(x + inc, y + inc, w - dec, h - dec), color0);
}


pub fn text(ctx: *UiContext, label: []const u8, x: f32, y: f32, w: f32, h: f32) void {
    const text_color = UiColor.c(1, 1, 1, 1);
    const f_offset = (h - ctx.font_h) * 0.5;
    draw_text(&ctx.al.allocator, label, x + f_offset, y + f_offset, ctx.font, text_color);
}


var text_fmt_buf = [_]u8{0} ** 256;
pub fn text_fmt(ctx: *UiContext, comptime fmt: []const u8, args: anytype, x: f32, y: f32, w: f32, h: f32) void {
    
    const t = std.fmt.bufPrint(&text_fmt_buf, fmt, args) catch "err";
    text(ctx, t, x, y, w, h);
}


pub fn box(x: f32, y: f32, w: f32, h: f32, z: f32) void {
    drawRectZ(UiRect.c(x, y, w, h), box_color_bg, z);
    drawRectZ(UiRect.c(x + 0.01, y + 0.01, w - 0.02, h - 0.02), box_color, z);
}


pub fn box_colors(x: f32, y: f32, w: f32, h: f32, z: f32, color_0: UiColor, color_1: UiColor) void {
    drawRectZ(UiRect.c(x, y, w, h), color_1, z);
    drawRectZ(UiRect.c(x + 0.01, y + 0.01, w - 0.02, h - 0.02), color_0, z);
}