pub const std = @import("std");
pub const Allocator = std.mem.Allocator;

pub usingnamespace @import("vector_math.zig");
pub const string = @import("string.zig");
pub const render = @import("render_opengl.zig");
pub const input = @import("input.zig");
pub const ztt = @import("ztt");
pub const plt = @import("../glfw_plataform.zig");
