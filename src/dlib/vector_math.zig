const std = @import("std");
const math = std.math;
const cast = @import("dutil.zig").cast;

const epsilon = 0.0001;

// Struct Declarations
pub const Color = struct {
    r: f32 = 0.0,
    g: f32 = 0.0,
    b: f32 = 0.0,
    a: f32 = 1.0,
    
    pub fn c(r: f32, g: f32, b: f32, a: f32) callconv(.Inline) Color {
        return Color{ .r = r, .g = g, .b = b, .a = a };
    }
    
    pub const white = Color.c(1,1,1,1);
    pub const red   = Color.c(1,0,0,1);
    pub const green = Color.c(0,1,0,1);
    pub const blue  = Color.c(0,0,1,1);
    pub const pink  = Color.c(1,0,1,1);
};

//~
pub const Vec2 = struct {
    x: f32 = 0.0,
    y: f32 = 0.0,
    
    pub fn c(x: f32, y: f32) callconv(.Inline) Vec2 {
        return Vec2{ .x = x, .y = y };
    }
    
    
    pub fn ca(a: [2]f32) callconv(.Inline) Vec2 {
        return Vec2{ .x = a[0], .y = a[1] };
    }
    
    pub fn neg(v: Vec2) callconv(.Inline) Vec2 {
        return Vec2{ .x = -v.x, .y = -v.y };
    }
};

pub fn Vec2_is_zero(v: Vec2) callconv(.Inline) bool {
    const result = v.x < epsilon and v.x > -epsilon and
        v.y < epsilon and v.y > -epsilon;
    return result;
}

pub fn Vec2_add(va: Vec2, vb: Vec2) Vec2 {
    const result = Vec2{
        .x = va.x + vb.x,
        .y = va.y + vb.y,
    };
    return result;
}

pub fn Vec2_sub(va: Vec2, vb: Vec2) Vec2 {
    const result = Vec2{
        .x = va.x - vb.x,
        .y = va.y - vb.y,
    };
    return result;
}

pub fn Vec2_mul(va: Vec2, vb: Vec2) Vec2 {
    const result = Vec2{
        .x = va.x * vb.x,
        .y = va.y * vb.y,
    };
    return result;
}

pub fn Vec2_div(va: Vec2, vb: Vec2) Vec2 {
    const result = Vec2{
        .x = va.x / vb.x,
        .y = va.y / vb.y,
    };
    return result;
}

pub fn Vec2_add_F(v: Vec2, f: f32) Vec2 {
    const result = Vec2{
        .x = v.x + f,
        .y = v.y + f,
    };
    return result;
}

pub fn Vec2_sub_F(v: Vec2, f: f32) Vec2 {
    const result = Vec2{
        .x = v.x - f,
        .y = v.y - f,
    };
    return result;
}

pub fn Vec2_mul_F(v: Vec2, f: f32) Vec2 {
    const result = Vec2{
        .x = v.x * f,
        .y = v.y * f,
    };
    return result;
}

pub fn Vec2_div_F(v: Vec2, f: f32) Vec2 {
    const result = Vec2{
        .x = v.x / f,
        .y = v.y / f,
    };
    return result;
}

pub fn Vec2_dot(va: Vec2, vb: Vec2) f32 {
    const result = va.x * vb.x + va.y * vb.y;
    return result;
}

pub fn Vec2_cross(va: Vec2, vb: Vec2) callconv(.Inline) f32 {
    const result = va.x * vb.y - va.y * vb.x;
    return result;
}

pub fn Vec2_len(v: Vec2) callconv(.Inline) f32 {
    const result = math.sqrt(v.x * v.x + v.y * v.y);
    return result;
}

pub fn Vec2_len_squared(v: Vec2) callconv(.Inline) f32 {
    const result = (v.x * v.x + v.y * v.y);
    return result;
}

pub fn Vec2_dist2(va: Vec2, vb: Vec2) callconv(.Inline) f32 {
    const result = Vec2_len_squared(Vec2_sub(va, vb));
    return result;
}

pub fn Vec2_dist(va: Vec2, vb: Vec2) callconv(.Inline) f32 {
    const result = Vec2_len(Vec2_sub(va, vb));
    return result;
}

pub fn Vec2_normalize(v: Vec2) Vec2 {
    const l = Vec2_len(v);
    const result = if (l > 0.001) Vec2_div_F(v, Vec2_len(v)) else Vec2{};
    return result;
}

pub fn Vec2_clamp_len(v: Vec2, min: f32, max: f32) Vec2 {
    const len = std.math.clamp(Vec2_len(v), min, max);
    const n = Vec2_normalize(v);
    const result = Vec2_mul_F(n, len);
    return result;
}

pub fn Vec2_angle_of(v: Vec2) f32 {
    var result = math.atan(v.y / v.x);
    if (v.x < 0) result += 3.1415926535;
    return result;
}


//~
pub const Vec3i = struct {
    x: i32 = 0,
    y: i32 = 0,
    z: i32 = 0,
    
    pub fn c(x: i32, y: i32, z: i32) callconv(.Inline) Vec3i {
        return Vec3i{ .x = x, .y = y, .z = z };
    }
};


pub fn Vec3i_eql(v0: Vec3i, v1: Vec3i) callconv(.Inline) bool {
    return (v0.x == v1.x and v0.y == v1.y and v0.z == v1.z);
}

//~
pub const Vec3 = struct {
    x: f32 = 0.0,
    y: f32 = 0.0,
    z: f32 = 0.0,
    
    pub fn c(x: f32, y: f32, z: f32) callconv(.Inline) Vec3 {
        return Vec3{ .x = x, .y = y, .z = z };
    }
    
    pub fn neg(v: *Vec3) callconv(.Inline) Vec3 {
        return Vec3{ .x = -v.x, .y = -v.y, .z = -v.z };
    }
};

pub fn Vec3_add(va: Vec3, vb: Vec3) Vec3 {
    const result = Vec3{
        .x = va.x + vb.x,
        .y = va.y + vb.y,
        .z = va.z + vb.z,
    };
    return result;
}

pub fn Vec3_sub(va: Vec3, vb: Vec3) Vec3 {
    const result = Vec3{
        .x = va.x - vb.x,
        .y = va.y - vb.y,
        .z = va.z - vb.z,
    };
    return result;
}

pub fn Vec3_mul(va: Vec3, vb: Vec3) Vec3 {
    const result = Vec3{
        .x = va.x * vb.x,
        .y = va.y * vb.y,
        .z = va.z * vb.z,
    };
    return result;
}

pub fn Vec3_div(va: Vec3, vb: Vec3) Vec3 {
    const result = Vec3{
        .x = va.x / vb.x,
        .y = va.y / vb.y,
        .z = va.z / vb.z,
    };
    return result;
}

pub fn Vec3_add_F(v: Vec3, f: f32) Vec3 {
    const result = Vec3{
        .x = v.x + f,
        .y = v.y + f,
        .z = v.z + f,
    };
    return result;
}

pub fn Vec3_sub_F(v: Vec3, f: f32) Vec3 {
    const result = Vec3{
        .x = v.x - f,
        .y = v.y - f,
        .z = v.z - f,
    };
    return result;
}

pub fn Vec3_mul_F(v: Vec3, f: f32) Vec3 {
    const result = Vec3{
        .x = v.x * f,
        .y = v.y * f,
        .z = v.z * f,
    };
    return result;
}

pub fn Vec3_div_F(v: Vec3, f: f32) Vec3 {
    const result = Vec3{
        .x = v.x / f,
        .y = v.y / f,
        .z = v.z / f,
    };
    return result;
}

pub fn Vec3_dot(va: Vec3, vb: Vec3) f32 {
    const result = va.x * vb.x + va.y * vb.y + va.z * vb.z;
    return result;
}

pub fn Vec3_cross(va: Vec3, vb: Vec3) Vec3 {
    var result = Vec3{};
    result.x = va.y * vb.z - va.z * vb.y;
    result.y = va.z * vb.x - va.x * vb.z;
    result.z = va.x * vb.y - va.y * vb.x;
    return result;
}

pub fn Vec3_len(v: Vec3) f32 {
    const result = math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    return result;
}

pub fn Vec3_normalize(v: Vec3) Vec3 {
    const l = Vec3_len(v);
    const result = if (l > 0.001) Vec3_div_F(v, Vec3_len(v)) else Vec3{};
    return result;
}

pub fn Vec3_rotate(v: Vec3, r: Rotor3) Vec3 {
    
    var q = Vec3{};
    q.x = r.a * v.x + v.y * r.bxy + v.z * r.bxz;
    q.y = r.a * v.y - v.x * r.bxy + v.z * r.byz;
    q.z = r.a * v.z - v.x * r.bxz - v.y * r.byz;
    
    const qxyz = v.x * r.byz - v.y * r.bxz + v.z * r.bxy;
    
    var res = Vec3{};
    res.x = r.a * q.x + q.y  * r.bxy + q.z  * r.bxz + qxyz * r.byz;
    res.y = r.a * q.y - q.x  * r.bxy - qxyz * r.bxz + q.z  * r.byz;
    res.z = r.a * q.z + qxyz * r.bxy - q.x  * r.bxz - q.y  * r.byz;
    
    return res;
}

pub fn Vec3_clamp_len(v: Vec3, min: f32, max: f32) Vec3 {
    const len = std.math.clamp(Vec3_len(v), min, max);
    const n = Vec3_normalize(v);
    const result = Vec3_mul_F(n, len);
    return result;
}

//~
pub const Bivec3 = struct {
    xy: f32,
    xz: f32,
    yz: f32,
    
    pub fn c(xy: f32, xz: f32, yz: f32) callconv(.Inline) Bivec3 {
        return Bivec3 {
            .xy = xy,
            .xz = xz,
            .yz = yz,
        };
    }
};

pub fn Bivec3_wedge(u: Vec3, v: Vec3) Bivec3 {
    const result = Vec3 {
        .xy = u.x * v.y - u.y * v.x,
        .xz = u.x * v.z - u.z * v.x,
        .yz = u.y * v.z - u.z * v.y,
    };
    return result;
}

pub const Rotor3 = struct {
    a: f32 = 1,
    bxy: f32 = 0,
    bxz: f32 = 0,
    byz: f32 = 0,
    
    pub fn c(a: f32, bxy: f32, bxz: f32, byz: f32) callconv(.Inline) Rotor3 {
        return Rotor3 {
            .a = a,
            .bxy = bxy,
            .bxz = bxz,
            .byz = byz,
        };
    }
    
    pub fn cb(a: f32, b: Bivec3) callconv(.Inline) Rotor3 {
        return Rotor3 {
            .a = a,
            .bxy = b.xy,
            .bxz = b.xz,
            .byz = b.yz,
        };
    }
    
    pub fn cp(plane: Bivec3, angle_rad: f32) Rotor3 {
        const sin_a = @sin(angle_rad * 0.5);
        const a = @cos(angle_rad * 0.5);
        
        const xy = -sin_a * plane.xy;
        const xz = -sin_a * plane.xz;
        const yz = -sin_a * plane.yz;
        
        return Rotor3 {
            .a = a,
            .bxy = xy,
            .bxz = xz,
            .byz = yz,
        };
    }
    
    pub fn reverse(r: Rotor3) callconv(.Inline) Rotor3 {
        return Rotor3 {
            .a = r.a,
            .bxy = -r.bxy,
            .bxz = -r.bxz,
            .byz = -r.byz,
        };
    }
    
    pub fn bivec(r: Rotor3) callconv(.Inline) Bivec3 {
        return Bivec3.c(r.bxy, r.bxz, r.byz);
    }
};

pub fn Rotor3_mul(p: Rotor3, q: Rotor3) callconv(.Inline) Rotor3 {
    var r = Rotor3{};
    
    r.a   = p.a   * q.a  -  p.bxy * q.bxy  -  p.bxz * q.bxz  -  p.byz * q.byz;
    r.bxy = p.bxy * q.a  +  p.a   * q.bxy  +  p.byz * q.bxz  -  p.bxz * q.byz;
    r.bxz = p.bxz * q.a  +  p.a   * q.bxz  -  p.byz * q.bxy  +  p.bxy * q.byz;
    r.byz = p.byz * q.a  +  p.a   * q.byz  +  p.bxz * q.bxy  -  p.bxy * q.bxz;
    
    return r;
}

pub fn Rotor3_rotate(p: Rotor3, q: Rotor3) callconv(.Inline) Rotor3 {
    const result = Rotor3_mul(Rotor3_mul(p, q), p.reverse());
    return result;
}

pub fn Rotor3_lenSqured(r: Rotor3) callconv(.Inline) f32 {
    const result = @sqrt(r.a) + @sqrt(r.bxy) + @sqrt(r.bxz) + @sqrt(r.byz);
    return result;
}

pub fn Rotor3_len(r: Rotor3) callconv(.Inline) f32 {
    const result = @sqrt(Rotor3_lenSqured(r));
    return result;
}

pub fn Rotor3_normalized(r: Rotor3) callconv(.Inline) Rotor3 {
    const one_over_l = 1.0 / Rotor3_len(r);
    const result = Rotor3{
        .a = r.a * one_over_l,
        .bxy = r.bxy * one_over_l,
        .bxz = r.bxz * one_over_l,
        .byz = r.byz * one_over_l,
    };
    return result;
}

pub const Vec4 = struct {
    x: f32 = 0.0,
    y: f32 = 0.0,
    z: f32 = 0.0,
    w: f32 = 0.0,
    
    pub fn c(x: f32, y: f32, z: f32, w: f32) callconv(.Inline) Vec4 {
        return Vec4{ .x = x, .y = y, .z = z, .w = w };
    }
};

pub const Rect = struct {
    x: f32 = 0.0,
    y: f32 = 0.0,
    w: f32 = 1.0,
    h: f32 = 1.0,
    
    pub fn c(x: f32, y: f32, w: f32, h: f32) callconv(.Inline) Rect {
        return Rect{ .x = x, .y = y, .w = w, .h = h };
    }
};

pub const Plane = struct {
    n: Vec3 = .{},
    d: f32 = .{},
    
    pub fn c(nx: f32, ny: f32, nz: f32, d: f32) callconv(.Inline) Plane {
        return Plane{
            .n = Vec3_normalize(Vec3.c(nx, ny, nz)),
            .d = d,
        };
    }
};

pub const Vertex = struct {
    pos: Vec3 = .{},
    uv: Vec2 = .{},
    w: f32 = 1.0,
    
    pub fn c(pos: Vec3, uv: Vec2) callconv(.Inline) Vertex {
        return Vertex{
            .pos = pos,
            .uv = uv,
        };
    }
};

pub const Transform = struct {
    position: Vec3 = .{},
    rotation: Rotor3 = .{},
    scale: Vec3 = .{
        .x = 1,
        .y = 1,
        .z = 1,
    },
};

pub fn lerp(a: f32, b: f32, t: f32) f32 {
    const result = (1 - t) * a + t * b;
    return result;
}

pub fn Color_lerp(ca: Color, cb: Color, t: f32) Color {
    const result = Color{
        .r = lerp(ca.r, cb.r, t),
        .g = lerp(ca.g, cb.g, t),
        .b = lerp(ca.b, cb.b, t),
        .a = lerp(ca.a, cb.a, t),
    };
    return result;
}

pub fn lineIntersectPlane(l_origin: Vec3, l_dir: Vec3, plane: Plane) ?Vec3 {
    var result: ?Vec3 = null;
    
    const denom = Vec3_dot(plane.n, l_dir);
    const epslon = 0.001;
    if (denom > epslon or denom < -epslon) {
        const t = (-plane.d - Vec3_dot(plane.n, l_origin)) / denom;
        const hit_pos = Vec3_add(l_origin, Vec3_mul_F(l_dir, t));
        result = hit_pos;
    }
    
    return result;
}

pub fn lineIntersectPlaneT(l_origin: Vec3, l_end: Vec3, plane: Plane, t: *f32) Vec3 {
    const ad = Vec3_dot(l_origin, plane.n);
    const bd = Vec3_dot(l_end, plane.n);
    t.* = (-plane.d - ad) / (bd - ad);
    const line_start_to_end = Vec3_sub(l_end, l_origin);
    const line_to_intersect = Vec3_mul_F(line_start_to_end, t.*);
    return Vec3_add(l_origin, line_to_intersect);
}

pub fn edgeFunction(xa: f32, ya: f32, xb: f32, yb: f32, xc: f32, yc: f32) callconv(.Inline) f32 {
    return (xc - xa) * (yb - ya) - (yc - ya) * (xb - xa);
}

pub fn edgeFunctionI(xa: i32, ya: i32, xb: i32, yb: i32, xc: i32, yc: i32) callconv(.Inline) i32 {
    const result = (xc -% xa) *% (yb -% ya) -% (yc -% ya) *% (xb -% xa);
    return result;
}

pub fn interpolateVertexAttr(va: Vertex, vb: Vertex, vc: Vertex, pos: Vec3) Vertex {
    var result = Vertex{
        .pos = pos,
    };
    
    const area = edgeFunction(va.pos.x, va.pos.y, vb.pos.x, vb.pos.y, vc.pos.x, vc.pos.y);
    
    var w0 = edgeFunction(vb.pos.x, vb.pos.y, vc.pos.x, vc.pos.y, pos.x, pos.y) / area;
    
    var w1 = edgeFunction(vc.pos.x, vc.pos.y, va.pos.x, va.pos.y, pos.x, pos.y) / area;
    var w2 = 1.0 - w0 - w1;
    
    if (false) {
        w0 /= va.w;
        w1 /= va.w;
        w2 /= va.w;
        const w_sum = w0 + w1 + w2;
        w0 /= w_sum;
        w1 /= w_sum;
        w2 /= w_sum;
    }
    
    result.color.r = w0 * va.color.r + w1 * vb.color.r + w2 * vc.color.r;
    result.color.g = w0 * va.color.g + w1 * vb.color.g + w2 * vc.color.g;
    result.color.b = w0 * va.color.b + w1 * vb.color.b + w2 * vc.color.b;
    result.color.a = w0 * va.color.a + w1 * vb.color.a + w2 * vc.color.a;
    
    return result;
}

pub fn baricentricCoordinates(a: anytype, b: anytype, c: anytype, p: anytype) Vec3 {
    if (@TypeOf(a) != Vec3 and @TypeOf(a) != Vec2) @compileError("");
    if (@TypeOf(b) != Vec3 and @TypeOf(b) != Vec2) @compileError("");
    if (@TypeOf(c) != Vec3 and @TypeOf(c) != Vec2) @compileError("");
    if (@TypeOf(p) != Vec3 and @TypeOf(p) != Vec2) @compileError("");
    
    const area = edgeFunction(a.x, a.y, b.x, b.y, c.x, c.y);
    var w0 = edgeFunction(b.x, b.y, c.x, c.y, p.x, p.y) / area;
    var w1 = edgeFunction(c.x, c.y, a.x, a.y, p.x, p.y) / area;
    var w2 = 1.0 - w0 - w1;
    return Vec3.c(w0, w1, w2);
}

pub fn rotateVectorOnY(v: Vec3, angle: f32) Vec3 {
    const result = Vec3{
        .x = v.x * @cos(angle) + v.z * @sin(angle),
        .y = v.y,
        .z = -v.x * @sin(angle) + v.z * @cos(angle),
    };
    return result;
}

pub fn rotateVectorOnX(v: Vec3, angle: f32) Vec3 {
    const result = Vec3{
        .x = v.x,
        .y = v.y * @cos(angle) + v.z * @sin(angle),
        .z = -v.y * @sin(angle) + v.z * @cos(angle),
    };
    return result;
}

pub fn rotateVectorOnZ(v: Vec3, angle: f32) Vec3 {
    const result = Vec3{
        .x = v.x * @cos(angle) + v.y * @sin(angle),
        .y = -v.x * @sin(angle) + v.y * @cos(angle),
        .z = v.z,
    };
    return result;
}

pub fn perspectiveMatrix(near: f32, far: f32, fov: f32, height_to_width_ratio: f32) [16]f32 {
    const _fov = fov * 3.1415926535 / 90.0;
    const S: f32 = @cos(_fov) / @sin(_fov);
    const s_h2w = S * height_to_width_ratio;
    const f1 = -(far / (far - near));
    const f2 = near * f1;
    
    const matrix = [16]f32{
        -s_h2w, 0, 0, 0,
        0, -S, 0, 0,
        0, 0, f1, -1,
        0, 0, f2, 0,
    };
    
    return matrix;
}

pub fn ortogonalMatrix(near: f32, far: f32, width: f32, height: f32) [16]f32 {
    
    const one_over_far_minus_near = 1 / (far - near);
    const f1 = -2.0 * one_over_far_minus_near;
    const f2 = -(far + near) * one_over_far_minus_near;
    
    const matrix = [16] f32 {
        1 / width, 0, 0, 0,
        0, 1 / height, 0, 0,
        0, 0, f1, 0,
        0, 0, f2, 1,
    };
    
    return matrix;
}

pub fn eulerAnglesToDirVector(v: Vec3) Vec3 {
    var result = Vec3{
        .x = -@sin(v.y),
        .y = -@sin(v.x) * @cos(v.y),
        .z = @cos(v.x) * @cos(v.y),
    };
    return result;
}

pub fn sphericalToCartesian(r: f32, z: f32, a: f32) Vec3 {
    const result = Vec3{
        .x = r * @sin(z) * @cos(a),
        .y = r * @sin(z) * @sin(a),
        .z = r * @cos(z),
    };
    return result;
}

const assert = @import("std").debug.assert;

pub fn Mat4_mul(m1: []const f32, m2: []const f32) [16]f32 {
    assert(m1.len == 16 and m2.len == 16);
    var r: [16]f32 = undefined;
    
    r[0] = m1[0] * m2[0] + m1[1] * m2[4] + m1[2] * m2[8]  + m1[3] * m2[12];
    r[1] = m1[0] * m2[1] + m1[1] * m2[5] + m1[2] * m2[9]  + m1[3] * m2[13];
    r[2] = m1[0] * m2[2] + m1[1] * m2[6] + m1[2] * m2[10] + m1[3] * m2[14];
    r[3] = m1[0] * m2[3] + m1[1] * m2[7] + m1[2] * m2[11] + m1[3] * m2[15];
    
    
    r[4] = m1[4] * m2[0] + m1[5] * m2[4] + m1[6] * m2[8]  + m1[7] * m2[12];
    r[5] = m1[4] * m2[1] + m1[5] * m2[5] + m1[6] * m2[9]  + m1[7] * m2[13];
    r[6] = m1[4] * m2[2] + m1[5] * m2[6] + m1[6] * m2[10] + m1[7] * m2[14];
    r[7] = m1[4] * m2[3] + m1[5] * m2[7] + m1[6] * m2[11] + m1[7] * m2[15];
    
    
    r[8]  = m1[8] * m2[0] + m1[9] * m2[4] + m1[10] * m2[8]  + m1[11] * m2[12];
    r[9]  = m1[8] * m2[1] + m1[9] * m2[5] + m1[10] * m2[9]  + m1[11] * m2[13];
    r[10] = m1[8] * m2[2] + m1[9] * m2[6] + m1[10] * m2[10] + m1[11] * m2[14];
    r[11] = m1[8] * m2[3] + m1[9] * m2[7] + m1[10] * m2[11] + m1[11] * m2[15];
    
    
    r[12] = m1[12] * m2[0] + m1[13] * m2[4] + m1[14] * m2[8]  + m1[15] * m2[12];
    r[13] = m1[12] * m2[1] + m1[13] * m2[5] + m1[14] * m2[9]  + m1[15] * m2[13];
    r[14] = m1[12] * m2[2] + m1[13] * m2[6] + m1[14] * m2[10] + m1[15] * m2[14];
    r[15] = m1[12] * m2[3] + m1[13] * m2[7] + m1[14] * m2[11] + m1[15] * m2[15];
    
    return r;
}

pub fn random_gradient(ix: i32, iy: i32) Vec2 {
    const x = cast(f32, ix);
    const y = cast(f32, iy);
    const random = 2920.0 * @sin(x * 21942.0 + y * 171324.0 + 8912.0) *
        @cos(x * 23157.0 * y * 217832.0 + 9758.0);
    return Vec2.c(@cos(random), @sin(random));
}


pub fn random_gradient_3d(ix: i32, iy: i32, iz: i32) Vec3 {
    const x = cast(f32, ix);
    const y = cast(f32, iy);
    const z = cast(f32, iz);
    
    // NOTE(Samuel): Sub optimal
    const random = z * 2920.0 * @sin(x * 21942.0 + y * 171324.0 + 8912.0) * @cos(x * 23157.0 * y * 217832.0 + 9758.0);
    const random2 = 2920.0 * @sin(x * 21942.0 + z * 171324.0 + 8912.0) * @cos(x * 23157.0 * z * 217832.0 + 9758.0);
    
    return Vec3.c(@cos(random), @sin(random), @sin(random2));
}


pub fn dot_grid_gradient(ix: i32, iy: i32, x: f32, y: f32) f32 {
    const gradient = random_gradient(ix, iy);
    
    const dx = x - cast(f32, ix);
    const dy = y - cast(f32, iy);
    
    return (dx * gradient.x + dy * gradient.y);
}

pub fn dot_grid_gradient_3d(ix: i32, iy: i32, iz: i32, x: f32, y: f32, z: f32) f32 {
    const gradient = random_gradient_3d(ix, iy);
    
    const dx = x - cast(f32, ix);
    const dy = y - cast(f32, iy);
    const dz = z - cast(f32, iz);
    
    return (dx * gradient.x + dy * gradient.y + dz * gradient.z);
}

pub fn smoothstep(a0: f32, a1: f32, w: f32) f32 {
    if (0.0 > w) return a0;
    if (1.0 < w) return a1;
    
    //return (a1 - a0) * w + a0;
    return (a1 - a0) * (3.0 - w * 2.0) * w * w + a0;
}

pub fn perlin_2d(x: f32, y: f32) f32 {
    const x0 = cast(i32, @floor(x));
    const x1 = x0 + 1;
    const y0 = cast(i32, @floor(y));
    const y1 = y0 + 1;
    
    const sx = x - cast(f32, x0);
    const sy = y - cast(f32, y0);
    
    var n0 = dot_grid_gradient(x0, y0, x, y);
    var n1 = dot_grid_gradient(x1, y0, x, y);
    const ix0 = smoothstep(n0, n1, sx);
    
    n0 = dot_grid_gradient(x0, y1, x, y);
    n1 = dot_grid_gradient(x1, y1, x, y);
    const ix1 = smoothstep(n0, n1, sx);
    
    const value = smoothstep(ix0, ix1, sy);
    return value;
}

// TODO(Samuel): Make perlin 3d
pub fn perlin_3d(x: f32, y: f32, z: f32) f32 {
    const result: f32 = 0.0;
    
    return result;
}

// Gleiston's Stuff

pub const cannonX = Vec2.c(1,0);
pub const cannonY = Vec2.c(0,1);

pub fn projVec2(u: Vec2, v: Vec2) callconv(.Inline) Vec2{// projection of 'v' onto 'u'
    return Vec2_mul_F(u, Vec2_dot(u,v)/Vec2_len_squared(u));
}

pub fn rotateVector2(v: Vec2, w: f32) Vec2{
    return Vec2.c(v.x*@cos(w)-v.y*@sin(w), v.x*@sin(w)+v.y*@cos(w));
}

pub fn Vec2_Reflect(v: Vec2, n: Vec2) Vec2{
    const proj = projVec2(n, neg(v));
    if(Vec2_dot(n, neg(v)) < 0 ){
        return Vec2.c(0,0);
    }
    const h2 = Vec2_mul_F(proj, 2);
    const reflect = Vec2_add(v, h2);
    return reflect;
}

//~ colision stuff
pub const Circle = struct {
    c: Vec2 = .{},
    r: f32 = 1.0,
    
    pub fn c(c: Vec2, r: f32) callconv(.Inline) Circle {
        return Circle{ .c = c, .r = r};
    }
};

pub const Line = struct {
    p0: Vec2 = .{},
    p1: Vec2 = .{},
    
    pub fn c(p0: Vec2, p1: Vec2) callconv(.Inline) Line {
        return Line{ .p0 = p0, .p1 = p0};
    }
};

pub fn projOnLine2(l: Line, p: Vec2) Vec2{// Projects the point 'p' onto the line 'l'
    const u = Vec2_sub(l.p1, l.p0);
    const v = Vec2_sub(p, l.p0);
    return Vec2_add(l.p0, projVec2(u,v));
}

pub fn projOnLineCap2(l: Line, p: Vec2) Vec2{// The projected point is always inside the line
    const u = projOnLine2(l, p);
    const v = Vec2_sub(l.p1, l.p0);
    const dot = Vec2_dot(Vec2_sub(u, l.p0), v);

    if(dot > Vec2_dot(v,v)) return l.p1
    else if(dot < 0) return l.p0
    else return u;
}

pub fn collision_Circle_Point(c: Circle, p: Vec2) callconv(.Inline) bool {
    const result = (c.c.x - p.x) * (c.c.x - p.x) + (c.c.y - p.y) * (c.c.y - p.y) <= c.r * c.r;
    return result;
}

pub fn collision_Circle(c1: Circle, c2: Circle) callconv(.Inline) bool {
    const result = (c1.c.x-c2.c.x)*(c1.c.x-c2.c.x) + (c1.c.y-c2.c.y)*(c1.c.y-c2.c.y) <=
        (c1.r+c2.r)*(c1.r+c2.r);
}

pub fn collision_Circle_Line(c: Circle, l: Line) bool{
    const v = projOnLineCap2(l, c.c);
    return collision_Circle_Point(c, v);
}

pub fn collision_Line_Point(l: Line, p: Vec2, tolerance: f32) bool{
    const result =
        Vec2_dist2(l.p0, p) + Vec2_dist2(l.p1, p) - tolerance
        <= Vec2_dist2(l.p0, l.p1);
    
    return result;
}

pub fn collision_Line(l1: Line, l2: Line) bool{
    const v1 = Vec2_sub(l1.p1, l1.p0);
    const v2 = Vec2_sub(l2.p1, l2.p0);
    const v3 = Vec2_sub(l1.p0, l2.p0);
    const denom = v1.x*v2.y-v1.y*v2.x;
    
    if (denom == 0)
        return collision_Line_Point(l1.l2.p0,0) or collision_Line_Point(l1.l2.p1,0);
    
    const a1 = (v1.x*v3.y-v1.y*v3.x)/denom;
    const a2 = (v2.x*v3.y-v2.y*v3.x)/denom;
    
    return a1 >= 0 and a1 <= 1 and
        a2 >= 0 and a2 <= 1;
}

pub fn collision_Rect_Point(r: Rect, w: f32, center: Vec2, p: Vec2) bool{
    p = Vec2_sub(p, center);
    p = rotateVector2(p, -w);
    p = Vec2_add(p, center);
    
    center = Vec2.c(center.x-r.w/2, center+r.h/2);
    
    return p.x > center.x and p.x < (center.x+r.w) and p.y > center.y and p.y > (center.y+r.h);
}


pub fn collision_Rect_Line(r: Rect, l: Line, center: Vec2) bool{
    if (collision_Rect_Point(r, l.p0, center) or collision_Rect_Point(r, l.p1, center)) {
        return true;
    } else {
        const side1 = Line {
            .p0 = Vec2.c(r.x, r.y),
            .p1 = Vec2.c(r.x + r.w, r.y),
        };
        
        const side2 = Line{
            .p0 = Vec2.c(r.x + r.w, r.y),
            .p1 = Vec2.c(r.x + r.w, r.y + r.h),
        };
        
        const side3 = Line{
            .p0 = Vec2.c(r.x + r.w, r.y + r.h),
            .p1 = Vec2.c(r.x, r.y + r.h),
        };
        const side4 = Line{
            .p0 = Vec2.c(r.x, r.y + r.h),
            .p1 = Vec2.c(r.x, r.y),
        };
        
        const result = collision_Line(side1,l) or collision_Line(side2,l) or collision_Line(side3,l) or collision_Line(side4,l);
        return result;
    }
}

pub fn collision_Circle_Rect(c: Circle, r: Rect, w: f32, center: Vec2) bool{
    var v: Vec2 = Vec2_sub(center, c.c);
    v = Vec2_normalize(v);
    v = Vec2_mul_F(v, c.r);
    
    return collision_Rect_Point(r, w, center, p);
}

// NOTE(Gleiston): Beyond this point the code is wrong
pub fn Collision_Rect(r1: Rect, r2: Rect) bool{
    return collision_Rect_Point(r1,Vec2{.x = r2.x, .y = r2.y}) or
        collision_Rect_Point(r1, Vec2{.x = r2.x + r2.w, .y = r2.y}) or
        collision_Rect_Point(r1, Vec2{.x = r2.x, .y = r2.y + r2.h}) or
        collision_Rect_Point(r1, Vec2{.x = r2.x + r2.w, .y = r2.y + r2.h});
}



