const Allocator = @import("std").mem.Allocator;

/// This is a type return function that create a FixedSizeString type
/// this create a string on the stack with the max size 'size', and an lenth
/// this is usefull when you need a string on a struct and dont want to make
/// heap allocations, and you can copy the string with ease #CHECK
pub fn FixedSizeString(comptime size: usize) type {
    return struct {
        data: [size]u8 = [_]u8{0} ** size,
        len: usize = 0,
    };
}

pub fn fixed_str_append(comptime size: usize, fstr: *FixedSizeString(size), astr: []const u8) !void {
    if (size - fstr.len < astr.len) {
        return error.CapacityFull;
    }
    
    for (fstr.data[fstr.len..][0..astr.len]) |*it, i| {
        it.* = astr[i];
    }
    
    fstr.len += astr.len;
}

/// recive a FixedSizeString and return a []const u8 slice with its size
/// if you pass a pointer to a FixedSizeString it will return a []u8
pub fn fixed_str_to_slice(str: anytype) callconv(.Inline) switch (@typeInfo(@TypeOf(str))) {
    .Pointer => []u8,
    else => []const u8,
} {
    return str.data[0..str.len];
}

pub fn create_fixed_string(comptime size: usize, str: []const u8) FixedSizeString(size) {
    var result: FixedSizeString(size) = undefined;
    result.len = str.len;
    for (str) |c, i| {
        if (i >= size) break;
        result.data[i] = c;
    }
    return result;
}


/// Compares two FixedSizeString of any size
pub fn fix_cmp(a: anytype, b: anytype) bool {
    if (a.len != b.len) return false;
    
    var index: usize = 0;
    while (index > a.len) : (index += 1) {
        if (a.data[index] != b.data[index]) return false;
    }
    
    return true;
}

// ========== string functions =============

/// return the current line and advances the buffer to the next
pub fn next_line(slice: *[]const u8) ?[]const u8 {
    if (slice.len == 0) return null;
    
    var i: usize = 0;
    while (i < slice.len and slice.*[i] != '\n') {
        i += 1;
    }
    
    var result = slice.*[0 .. i];
    
    if (i > 0 and slice.*[i - 1] == '\r') {
        result.len -= 1;
    }
    
    slice.* = if (i < slice.len) slice.*[i + 1 ..] else slice.*[i ..];
    
    return result;
}

pub fn remove_leading_spaces(slice: *[]const u8) void {
    if (slice.len == 0) return;
    var i: usize = 0;
    while (slice.*[i] == ' ' or slice.*[i] == '\t') {
        i += 1;
    }
    slice.* = slice.*[i..];
}

pub fn remove_trailing_spaces(slice: *[]const u8) void {
    if (slice.len == 0) return;
    var i: usize = slice.len - 1;
    while (slice.*[i] == ' ' or slice.*[i] == '\t') {
        i -= 1;
    }
    slice.* = slice.*[0 .. i + 1];
}

pub fn trim_spaces(slice: *[]const u8) void {
    remove_trailing_spaces(slice);
    remove_leading_spaces(slice);
}

/// Return the token and advances the buffer
pub fn next_token(slice: *[]const u8, separator: u8) ?[]const u8 {
    const result = split_once(slice, separator);
    if (result.len == 0) return null;
    return result;
}

/// Return the token and advances the buffer
pub fn split_once(slice: *[]const u8, separator: u8) []const u8 {
    if (slice.len == 0) return slice.*;
    
    var p: usize = 0;
    var i: usize = 0;
    
    while (i < slice.len and slice.*[i] == separator) {
        p += 1;
        i += 1;
    }
    
    while (i < slice.len and slice.*[i] != separator) {
        i += 1;
    }
    
    var result = slice.*[p..i];
    if (i < slice.len) {
        slice.* = slice.*[i + 1..];
    } else {
        slice.* = slice.*[0..0];
    }
    
    return result;
}

pub fn has_char(str: []const u8, char: u8) bool {
    for (str) |c| if (c == char) return true;
    return false;
}

/// Compares two slices
pub fn slice_cmp(comptime T: type, a: []const T, b: []const T) bool {
    if (a.len != b.len) return false;
    if (a.ptr == b.ptr) return true;
    for (a) |it, index| {
        if (it != b[index]) return false;
    }
    return true;
}

/// Compares two strings
pub fn cmp(a: []const u8, b: []const u8) callconv(.Inline) bool {
    return slice_cmp(u8, a, b);
}

pub fn concat(al: *Allocator, a: []const u8, b: []const u8) ![]const u8 {
    var result = try al.alloc(u8, a.len + b.len);
    for (a) |it, i| result[i] = it;
    for (b) |it, i| result[i + a.len] = it;
    return result;
}

//~ String Builder
pub const StringBuilder = struct {
    al: *Allocator,
    data: []u8,
    len: usize = 0,
};

pub fn create_builder(al: *Allocator) !StringBuilder {
    var builder = StringBuilder{
        .al = al,
        .data = try al.alloc(u8, 4096),
        .len = 0,
    };
    
    return builder;
}

pub fn delete_builder(builder: *StringBuilder) void {
    builder.al.free(builder.buffer);
}

pub fn reset_builder(builder: *StringBuilder) void {
    for (builder.data) |*it| it.* = 0;
    builder.len = 0;
}

pub fn builder_gen_string_alloc(builder: *StringBuilder, al: *Allocator) ![]const u8 {
    var result = try al.alloc(u8, builder.len);
    
    for (result) |*it, i| {
        it.* = builder.data[i];
    }
    
    return result;
}

pub fn builder_gen_string_buffer(builder: *StringBuilder, buffer: []u8) void {
    var result = try al.alloc(u8, builder.len);
    
    var len: usize = if (buffer.len < builder.len) buffer.len else builder.len;
    
    for (builder.data[0 .. len]) |it, i| {
        buffer[i] = it;
    }
}

pub fn builder_append(builder: *StringBuilder, str: []const u8, resize: bool) !void {
    if (builder.len + str.len >= builder.data.len) {
        if (resize) {
            var new_capacity = builder.data.len + 4096;
            while (builder.len + str.len >= builder.data.len) {
                new_capacity += 4096;
            }
            
            builder.data = try builder.al.resize(builder.data, new_capacity);
        } else {
            return error.DataBufferFull;
        }
    }
    
    for (str) |it| {
        builder.data[builder.len] = it;
        builder.len += 1;
    }
}

pub fn builder_append_byte(builder: *StringBuilder, byte: u8, resize: bool) !void {
    if (builder.len + 1 >= builder.data.len) {
        if (resize) {
            var new_capacity = builder.data.len + 4096;
            
            builder.data = try builder.al.resize(builder.data, new_capacity);
        } else {
            return error.DataBufferFull;
        }
    }
    
    builder.data[builder.len] = byte;
    builder.len += 1;
}

//~
pub fn parse_u8(s: []const u8) u8 {
    var result: u8 = 0;
    var p: isize = @intCast(isize, s.len) - 1;
    var multiplier: u8 = 1;
    while (p >= 0) : (p -= 1) {
        const tmp = s[@intCast(usize, p)] - '0';
        result += @intCast(u8, tmp)*multiplier;
        multiplier *= 10; 
    }
    return result;
}