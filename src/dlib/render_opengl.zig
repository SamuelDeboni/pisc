usingnamespace @import("dlib.zig");
const gl = @import("gl.zig");

pub const ShaderProgram = struct {
    handle: u32 = 0,
};

var basic_shader = ShaderProgram{};
var texture_shader = ShaderProgram{};
var font_shader = ShaderProgram{};
var mesh_shader = ShaderProgram{};
var chunk_shader = ShaderProgram{};
var lux_shader = ShaderProgram{};

pub const ShaderType = enum {
    fragment,
    vertex,
};

pub const Font = struct {
    texture: Texture = .{},
    first_char: u32 = 0,
    num_chars: u32 = 0,
    char_data: []ztt.BakedChar = undefined,
};

pub const Camera = struct {
    transform: Transform = .{
        //.rotation = Rotor3.cp(Bivec3.c(1, 0, 0), 3.14 * 0.25),
    },
    
    near: f32 = 0.1,
    far: f32 = 1000.0,
    fov: f32 = 70.0,
    
    ortogonal: bool = false,
    size: f32 = 10.0,
};

pub fn get_camera_matrix(cam: Camera) [16]f32 {
    const cp = camera.transform.position;
    
    const v0 = Vec3_rotate(Vec3.c(1, 0, 0), camera.transform.rotation);
    const v1 = Vec3_rotate(Vec3.c(0, 1, 0), camera.transform.rotation);
    const v2 = Vec3_rotate(Vec3.c(0, 0, 1), camera.transform.rotation);
    
    const rot_mat = [16]f32 {
        v0.x, v1.x, v2.x, 0,
        v0.y, v1.y, v2.y, 0,
        v0.z, v1.z, v2.z, 0,
        0,    0,    0,    1,
    };
    const mov_mat = [16]f32 {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        -cp.x, -cp.y, -cp.z, 1,
    };
    
    return Mat4_mul(mov_mat[0..], rot_mat[0..]);
}

pub fn camera_size() Vec2 {
    const h2w = @intToFloat(f32, plt.window.height) /
        @intToFloat(f32, plt.window.width);
    
    return Vec2.c(camera.size, camera.size * h2w);
}

pub fn projection_matrix_from_camera(cam: Camera) [16]f32 {
    const h2w = @intToFloat(f32, plt.window.height) /
        @intToFloat(f32, plt.window.width);
    
    if (cam.ortogonal) {
        return ortogonalMatrix(cam.near, cam.far, cam.size, cam.size * h2w);
    } else {
        return perspectiveMatrix(cam.near, cam.far, cam.fov, h2w);
    }
}

pub const Texture = struct {
    width: u32 = 0,
    height: u32 = 0,
    handle: u32 = 0,
};

pub const LuxTexture = struct {
    p_width: u32 = 0,
    p_count: u32 = 0,
    l_width: u32 = 0,
    l_height: u32 = 0,
    
    p_handle: u32 = 0,
    m_handle: u32 = 0,
    a_handle: u32 = 0,
    n_handle: u32 = 0,
};

pub const TextureArray = struct {
    width: u32 = 0,
    height: u32 = 0,
    depth: u32 = 0,
    handle: u32 = 0,
};

pub fn bake_font(al: *Allocator, font_size: f32, first_char: u32, num_chars: u32, ttf_data: [*:0]const u8) !Font {
    
    const ztt_font = try ztt.bake_font_alloc(plt.g_allocator, font_size, first_char, num_chars, ttf_data);
    
    var font = Font{};
    font.texture = render.upload_image_bitmap(ztt_font.bitmap, ztt_font.bitmap_w, ztt_font.bitmap_h);
    font.first_char = ztt_font.first_char;
    font.num_chars = ztt_font.num_chars;
    font.char_data = ztt_font.char_data;
    
    al.free(ztt_font.bitmap);
    
    return font;
}

pub fn upload_lux(lux: LuxFile) LuxTexture {
    var result = LuxTexture{
        .p_width = lux.header.p_width,
        .p_count = lux.header.p_count,
        .l_width  = lux.header.l_width,
        .l_height = lux.header.l_height,
    };
    
    var handles = [_]u32{0, 0, 0, 0};
    
    gl.genTextures(4, @ptrCast([*]u32, &handles[0]));
    
    result.p_handle = handles[0];
    result.m_handle = handles[1];
    result.a_handle = handles[2];
    result.n_handle = handles[3];
    
    gl.activeTexture(gl.TEXTURE0);
    
    {
        gl.bindTexture(gl.TEXTURE_2D, result.p_handle);
        
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,
                      cast(c_int, result.p_width),
                      cast(c_int, result.p_count),
                      0, gl.RGBA, gl.UNSIGNED_BYTE,
                      @ptrCast(*const c_void, lux.p_data.ptr));
    }
    
    assert_gl("Upload Lux Pallete");
    
    {
        gl.bindTexture(gl.TEXTURE_2D, result.m_handle);
        
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RED,
                      cast(c_int, result.l_width),
                      cast(c_int, result.l_height),
                      0, gl.RED, gl.UNSIGNED_BYTE, 
                      @ptrCast(*const c_void, lux.m_data.ptr));
    }
    
    assert_gl("Upload Lux Material");
    
    gl.bindTexture(gl.TEXTURE_2D, result.a_handle);
    
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RED,
                  cast(c_int, result.l_width),
                  cast(c_int, result.l_height),
                  0, gl.RED, gl.UNSIGNED_BYTE, 
                  @ptrCast(*const c_void, lux.a_data.ptr));
    
    assert_gl("Upload Lux atenuation");
    
    gl.bindTexture(gl.TEXTURE_2D, result.n_handle);
    
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB,
                  cast(c_int, result.l_width),
                  cast(c_int, result.l_height),
                  0, gl.RGB, gl.UNSIGNED_BYTE,
                  @ptrCast(*const c_void, lux.n_data.ptr));
    
    assert_gl("Upload Lux normal");
    
    return result;
}

pub fn upload_image(image: Image) Texture {
    var result = Texture{
        .width = image.width,
        .height = image.height,
    };
    gl.genTextures(1, @ptrCast([*]u32, &result.handle));
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, result.handle);
    
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, @intCast(c_int, image.width), @intCast(c_int, image.height), 0, gl.RGBA, gl.UNSIGNED_BYTE, @ptrCast(*const c_void, image.raw.ptr));
    //gl.generateMipmap(gl.TEXTURE_2D);
    
    return result;
}

pub fn upload_image_array(image: ImageArray) TextureArray {
    var result = TextureArray{
        .width = image.width,
        .height = image.height,
        .depth = image.depth,
    };
    
    gl.activeTexture(gl.TEXTURE0);
    
    gl.genTextures(1, @ptrCast([*]u32, &result.handle));
    gl.bindTexture(gl.TEXTURE_2D_ARRAY, result.handle);
    
    gl.texImage3D(gl.TEXTURE_2D_ARRAY,
                  0,
                  gl.RGBA,
                  cast(c_int, image.width),
                  cast(c_int, image.height),
                  cast(c_int, image.depth),
                  0,
                  gl.RGBA,
                  gl.UNSIGNED_BYTE,
                  @ptrCast(*const c_void, image.raw.ptr));
    
    gl.generateMipmap(gl.TEXTURE_2D_ARRAY);
    
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MAX_LEVEL, 4);
    
    gl.texParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    gl.texParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    
    assert_gl("Create Texture Array");
    
    return result;
}

pub fn upload_image_bitmap(bitmap: []const u8, w: u32, h: u32) Texture {
    var result = Texture{
        .width = w,
        .height = h,
    };
    
    gl.genTextures(1, @ptrCast([*]u32, &result.handle));
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, result.handle);
    
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RED, @intCast(c_int, w), @intCast(c_int, h), 0, gl.RED, gl.UNSIGNED_BYTE, @ptrCast(*const c_void, bitmap.ptr));
    gl.generateMipmap(gl.TEXTURE_2D);
    
    return result;
}

pub fn delete_texture(texture: Texture) void {
    gl.deleteTextures(1, @ptrCast([*]const u32, &texture.handle));
}

pub fn compile_shader(source: []const u8, shader_type: ShaderType) u32 {
    var result: u32 = gl.createShader(switch (shader_type) {
                                          .vertex => gl.VERTEX_SHADER,
                                          .fragment => gl.FRAGMENT_SHADER,
                                      });
    
    const shader_list = [_] [*]const u8 { @ptrCast([*]const u8, &source[0]) };
    const size_list = [_]i32 { @intCast(i32, source.len) };
    
    gl.shaderSource(result, 1,
                    @ptrCast([*]const [*]const u8, &shader_list[0]),
                    @ptrCast([*]const i32, &size_list[0]));
    
    gl.compileShader(result);
    
    var success: u32 = 0;
    var info_log = [_]u8{0} ** 512;
    gl.getShaderiv(result, gl.COMPILE_STATUS, &success);
    if (success == 0) {
        var len: c_int = 0;
        gl.getShaderInfoLog(result, 512, &len, @ptrCast([*]u8, &info_log[0]));
        std.debug.print("{s}\n\n{s}\n", .{source, info_log[0..@intCast(usize, len)]});
        @panic("Error compiling shader");
    }
    
    return result;
}

pub fn create_shader_program(vertex: []const u8, fragment: []const u8) ShaderProgram {
    const vert = compile_shader(vertex, .vertex);
    defer gl.deleteShader(vert);
    
    const frag = compile_shader(fragment, .fragment);
    defer gl.deleteShader(frag);
    
    const prog: u32 = gl.createProgram();
    gl.attachShader(prog, vert);
    gl.attachShader(prog, frag);
    gl.linkProgram(prog);
    
    
    var info_log = [_]u8{0} ** 512;
    if (std.builtin.mode == .Debug){
        var len: c_int = 0;
        gl.getProgramInfoLog(prog, 512, &len, @ptrCast([*]u8, &info_log[0]));
        if (len > 0) {
            std.debug.print("[Render] shader program log: {s}", .{info_log[0..@intCast(usize, len)]});
        }
    }
    
    
    return ShaderProgram {
        .handle = prog,
    };
}

pub const Mesh = struct {
    vao: u32 = 0,
    vbo: u32 = 0,
    ebo: u32 = 0,
    i_count: u32 = 0,
};

pub fn delete_mesh(mesh: *Mesh) void {
    gl.deleteBuffers(1, @ptrCast([*]u32, &mesh.vbo));
    gl.deleteBuffers(1, @ptrCast([*]u32, &mesh.ebo));
    gl.deleteVertexArrays(1, @ptrCast([*]u32, &mesh.vao));
    
    mesh.vao = 0;
    mesh.vbo = 0;
    mesh.ebo = 0;
    mesh.i_count = 0;
}

pub fn update_mesh(mesh: *Mesh, vertices: []const f32, indices: []const u32) void {
    gl.bindVertexArray(mesh.vao);
    
    gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, @intCast(isize, vertices.len * @sizeOf(f32)), @ptrCast(?*const c_void, vertices.ptr), gl.DYNAMIC_DRAW);
    
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ebo);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, @intCast(isize, indices.len * @sizeOf(u32)), @ptrCast(?*const c_void, indices.ptr), gl.DYNAMIC_DRAW);
    
    mesh.i_count = @intCast(u32, indices.len);
}

pub fn update_mesh_chunk(mesh: *Mesh, vertices: []const ChunkVert, indices: []const u32) void {
    gl.bindVertexArray(mesh.vao);
    
    gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, @intCast(isize, vertices.len * @sizeOf(ChunkVert)), @ptrCast(?*const c_void, vertices.ptr), gl.DYNAMIC_DRAW);
    
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ebo);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, @intCast(isize, indices.len * @sizeOf(u32)), @ptrCast(?*const c_void, indices.ptr), gl.DYNAMIC_DRAW);
    
    mesh.i_count = @intCast(u32, indices.len);
}


// Create a mesh with a given vertex data
// vertex format:
// ppp - tt - nnn
pub fn create_mesh_options(vertices: []const f32, indices: []const u32, dynamic: bool) Mesh {
    var vbo: u32 = 0;
    var ebo: u32 = 0;
    var vao: u32 = 0;
    
    gl.genBuffers(1, @ptrCast([*]u32, &vbo));
    gl.genBuffers(1, @ptrCast([*]u32, &ebo));
    gl.genVertexArrays(1, @ptrCast([*]u32, &vao));
    
    gl.bindVertexArray(vao);
    
    const draw_type: gl.GLenum = if (dynamic) gl.DYNAMIC_DRAW else gl.STATIC_DRAW;
    
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
    gl.bufferData(gl.ARRAY_BUFFER, @intCast(isize, vertices.len * @sizeOf(f32)), @ptrCast(?*const c_void, vertices.ptr), draw_type);
    
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, @intCast(isize, indices.len * @sizeOf(u32)), @ptrCast(?*const c_void, indices.ptr), draw_type);
    
    gl.vertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, 8 * @sizeOf(f32), @intToPtr(?*const c_void, 0));
    gl.enableVertexAttribArray(0);
    
    gl.vertexAttribPointer(1, 2, gl.FLOAT, gl.FALSE, 8 * @sizeOf(f32), @intToPtr(?*const c_void, 3 * @sizeOf(f32)));
    gl.enableVertexAttribArray(1);
    
    gl.vertexAttribPointer(2, 3, gl.FLOAT, gl.FALSE, 8 * @sizeOf(f32), @intToPtr(?*const c_void, 5 * @sizeOf(f32)));
    gl.enableVertexAttribArray(2);
    
    assert_gl("Create Mesh Options");
    return Mesh {
        .vao = vao,
        .vbo = vbo,
        .ebo = ebo,
        .i_count = @intCast(u32, indices.len)
    };
    
}

pub fn create_mesh_chunk(vertices: []const ChunkVert, indices: []const u32) Mesh {
    var vbo: u32 = 0;
    var ebo: u32 = 0;
    var vao: u32 = 0;
    
    gl.genBuffers(1, @ptrCast([*]u32, &vbo));
    gl.genBuffers(1, @ptrCast([*]u32, &ebo));
    gl.genVertexArrays(1, @ptrCast([*]u32, &vao));
    
    gl.bindVertexArray(vao);
    
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
    gl.bufferData(gl.ARRAY_BUFFER, @intCast(isize, vertices.len * @sizeOf(ChunkVert)), @ptrCast(?*const c_void, vertices.ptr), gl.DYNAMIC_DRAW);
    
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, @intCast(isize, indices.len * @sizeOf(u32)), @ptrCast(?*const c_void, indices.ptr), gl.DYNAMIC_DRAW);
    
    gl.vertexAttribPointer(0, 2, gl.UNSIGNED_BYTE, gl.FALSE, 11, @intToPtr(?*const c_void, 0));
    gl.enableVertexAttribArray(0);
    
    gl.vertexAttribPointer(1, 1, gl.UNSIGNED_SHORT, gl.FALSE, 11, @intToPtr(?*const c_void, 2));
    gl.enableVertexAttribArray(1);
    
    gl.vertexAttribPointer(2, 2, gl.BYTE, gl.FALSE, 11, @intToPtr(?*const c_void, 4));
    gl.enableVertexAttribArray(2);
    
    gl.vertexAttribPointer(3, 3, gl.BYTE, gl.TRUE, 11, @intToPtr(?*const c_void, 6));
    gl.enableVertexAttribArray(3);
    
    gl.vertexAttribPointer(4, 1, gl.UNSIGNED_SHORT, gl.FALSE, 11, @intToPtr(?*const c_void, 9));
    gl.enableVertexAttribArray(4);
    assert_gl("Create Mesh Chunk");
    
    return Mesh {
        .vao = vao,
        .vbo = vbo,
        .ebo = ebo,
        .i_count = @intCast(u32, indices.len)
    };
    
}

pub fn create_mesh(vertices: []const f32, indices: []const u32) Mesh {
    return create_mesh_options(vertices, indices, false);
}

pub fn create_dynamic_mesh(vertices: []const f32, indices: []const u32) Mesh {
    return create_mesh_options(vertices, indices, true);
}



const quad_vertices = [_]f32{
    1.0, 1.0, 0.0,  // top right
    1.0, 0.0, 0.0,  // bottom right
    0.0, 0.0, 0.0,  // bottom left
    0.0, 1.0, 0.0,   // top left
};

const quad_indices = [_]u32{  // note that we start from 0!
    0, 1, 3,   // first triangle
    1, 2, 3,    // second triangle
};

const font_quad_indices = [_]u32{  // note that we start from 0!
    0, 3, 1,   // first triangle
    1, 3, 2,    // second triangle
};

var quad_vbo: u32 = 0;
var quad_ebo: u32 = 0;
var quad_vao: u32 = 0;

var font_vbo: u32 = 0;
var font_ebo: u32 = 0;
var font_vao: u32 = 0;

fn assert_gl(caller: []const u8) void {
    const err = gl.getError();
    if (err != 0) {
        std.debug.print("[Render] Opengl error {} on {s}\n", .{err, caller});
        unreachable;
    }
}

pub fn init() void {
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    
    gl.enable(gl.CULL_FACE);
    gl.cullFace(gl.FRONT);
    
    
    if (std.builtin.mode == .Debug) {
        gl.enable(gl.DEBUG_OUTPUT);
        gl.enable(gl.DEBUG_OUTPUT_SYNCHRONOUS);
        gl.debugMessageCallback(gl.debugOutput, null);
        gl.debugMessageControl(gl.DONT_CARE, gl.DONT_CARE, gl.DONT_CARE, 0, undefined, gl.TRUE);
    }
    
    gl.frontFace(gl.CCW);
    {
        gl.genBuffers(1, @ptrCast([*]u32, &quad_vbo));
        gl.genBuffers(1, @ptrCast([*]u32, &quad_ebo));
        gl.genVertexArrays(1, @ptrCast([*]u32, &quad_vao));
        
        gl.bindVertexArray(quad_vao);
        
        gl.bindBuffer(gl.ARRAY_BUFFER, quad_vbo);
        gl.bufferData(gl.ARRAY_BUFFER, quad_vertices.len * @sizeOf(f32), @ptrCast(*const c_void, &quad_vertices[0]), gl.STATIC_DRAW);
        
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, quad_ebo);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, quad_indices.len * @sizeOf(u32), @ptrCast(*const c_void, &quad_indices[0]), gl.STATIC_DRAW);
        
        gl.vertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, 3 * @sizeOf(f32), @intToPtr(?*const c_void, 0));
        gl.enableVertexAttribArray(0);
    }
    
    {
        gl.genBuffers(1, @ptrCast([*]u32, &font_vbo));
        gl.genBuffers(1, @ptrCast([*]u32, &font_ebo));
        gl.genVertexArrays(1, @ptrCast([*]u32, &font_vao));
        
        gl.bindVertexArray(font_vao);
        
        gl.bindBuffer(gl.ARRAY_BUFFER, font_vbo);
        gl.bufferData(gl.ARRAY_BUFFER, quad_vertices.len * @sizeOf(f32), @ptrCast(*const c_void, &quad_vertices[0]), gl.STATIC_DRAW);
        
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, font_ebo);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, font_quad_indices.len * @sizeOf(u32), @ptrCast(*const c_void, &font_quad_indices[0]), gl.STATIC_DRAW);
        
        gl.vertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, 3 * @sizeOf(f32), @intToPtr(?*const c_void, 0));
        gl.enableVertexAttribArray(0);
    }
    
    basic_shader = create_shader_program(@embedFile("shaders/shader.vert"), @embedFile("shaders/shader.frag"));
    
    texture_shader = create_shader_program(@embedFile("shaders/tex_shader.vert"), @embedFile("shaders/tex_shader.frag"));
    lux_shader = create_shader_program(@embedFile("shaders/lux_shader.vert"), @embedFile("shaders/lux_shader.frag"));
    
    mesh_shader = create_shader_program(@embedFile("shaders/mesh_shader.vert"), @embedFile("shaders/mesh_shader.frag"));
    
    chunk_shader =
        create_shader_program(@embedFile("shaders/chunk_shader.vert"), @embedFile("shaders/chunk_shader.frag"));
    
    font_shader = create_shader_program(@embedFile("shaders/font_shader.vert"), @embedFile("shaders/font_shader.frag"));
    
    assert_gl("Init");
}

pub fn clear(r: f32, g: f32, b: f32) void {
    gl.clearColor(r, g, b, 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
}

pub fn resize_viewport(x: i64, y: i64, w: i64, h: i64) void {
    gl.viewport(@intCast(i32, x),
                @intCast(i32, y),
                @intCast(c_int, w),
                @intCast(c_int, h));
}

//~ Drawing functions

pub var camera = Camera{
    .transform = .{
        .position = Vec3.c(0, 0, 10),
    },
};

fn gen_line_element(comptime len: u32) [len]u32 {
    var result: [len]u32 = undefined;
    const size = len / 2;
    
    var i: u32 = 0;
    
    while (i < size) : (i += 1) {
        result[i * 2] = i;
        if (i < size - 1) {
            result[(i * 2) + 1] = i + 1;
        }
    }
    
    return result;
}

const line_indices = gen_line_element(512);


pub fn draw_line(xa: f32, ya: f32, xa: f32, ya: f32, color: Color) void {
    const lines = [_][3]f32 {
        .{xa, ya, 0.0},
        .{xb, yb, 0.0},
    };
    render.draw_lines(&lines, color);
}

pub fn draw_line_v(va: Vec2, vb: Vec2, color: Color) void {
    const lines = [_][3]f32 {
        .{va.x, va.y, 0.0},
        .{vb.x, vb.y, 0.0},
    };
    render.draw_lines(&lines, color);
}

// TODO(samuel): Pre generate the buffers and only update then
pub fn draw_lines(l: []const [3]f32, color: Color) void {
    const e_count = l.len * 2 - 1;
    if (e_count > line_indices.len) return;
    
    var line_vbo: u32 = 0;
    var line_ebo: u32 = 0;
    var line_vao: u32 = 0;
    
    // NOTE(samuel): Create vao
    {
        gl.genBuffers(1, @ptrCast([*]u32, &line_vbo));
        gl.genBuffers(1, @ptrCast([*]u32, &line_ebo));
        gl.genVertexArrays(1, @ptrCast([*]u32, &line_vao));
        
        gl.bindVertexArray(line_vao);
        
        gl.bindBuffer(gl.ARRAY_BUFFER, line_vbo);
        gl.bufferData(gl.ARRAY_BUFFER, cast(isize, l.len * @sizeOf(f32) * 3),
                      @ptrCast(*const c_void, &l[0][0]), gl.STREAM_DRAW);
        
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, line_ebo);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, cast(isize, e_count * @sizeOf(u32)), @ptrCast(*const c_void, &line_indices[0]), gl.STATIC_DRAW);
        
        
        gl.vertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, 3 * @sizeOf(f32),
                               @intToPtr(?*const c_void, 0));
        
        gl.enableVertexAttribArray(0);
    }
    
    // NOTE(samuel): Draw lines
    {
        const shader = basic_shader;
        gl.useProgram(shader.handle);
        
        gl.uniform4f(gl.getUniformLocation(shader.handle, "tint"),
                     color.r, color.g, color.b, color.a);
        
        const projection = projection_matrix_from_camera(camera);
        gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "projection"),
                            1, gl.FALSE, @ptrCast([*]const f32, &projection[0]));
        
        const view = get_camera_matrix(camera);
        
        gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "view"),
                            1, gl.FALSE, @ptrCast([*]const f32, &view[0]));
        
        const model = [16]f32{
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
        };
        
        gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "model"),
                            1, gl.FALSE, @ptrCast([*]const f32, &model[0]));
        
        gl.drawElements(gl.LINES, cast(c_int, e_count), gl.UNSIGNED_INT, null);
    }
    
    gl.bindBuffer(gl.ARRAY_BUFFER, 0);
    
    gl.deleteBuffers(1, @ptrCast([*]u32, &line_vbo));
    gl.deleteBuffers(1, @ptrCast([*]u32, &line_ebo));
    gl.deleteVertexArrays(1, @ptrCast([*]u32, &line_vao));
    
    assert_gl("Draw Solid Lines");
}

pub fn draw_chunk(mesh: Mesh, transform: Transform, texture: TextureArray) void {
    const shader = chunk_shader;
    gl.useProgram(shader.handle);
    
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D_ARRAY, texture.handle);
    gl.uniform1i(gl.getUniformLocation(shader.handle, "tex"), 0);
    
    gl.uniform4f(gl.getUniformLocation(shader.handle, "tint"),
                 1, 1, 1, 1);
    
    const projection = projection_matrix_from_camera(camera);
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "projection"),
                        1, gl.FALSE, @ptrCast([*]const f32, &projection[0]));
    
    const view = get_camera_matrix(camera);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "view"),
                        1, gl.FALSE, @ptrCast([*]const f32, &view[0]));
    
    const model = [16]f32{
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        transform.position.x, transform.position.y, transform.position.z, 1,
    };
    
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "model"),
                        1, gl.FALSE, @ptrCast([*]const f32, &model[0]));
    
    gl.bindVertexArray(mesh.vao);
    gl.drawElements(gl.TRIANGLES, @intCast(c_int, mesh.i_count), gl.UNSIGNED_INT, null);
    
    assert_gl("Draw Chunk");
}

pub fn draw_mesh(mesh: Mesh, transform: Transform, texture: Texture) void {
    const shader = mesh_shader;
    gl.useProgram(shader.handle);
    
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture.handle);
    
    gl.uniform1i(gl.getUniformLocation(shader.handle, "tex"), 0);
    gl.uniform4f(gl.getUniformLocation(shader.handle, "tint"),
                 1, 1, 1, 1);
    
    const projection = projection_matrix_from_camera(camera);
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "projection"),
                        1, gl.FALSE, @ptrCast([*]const f32, &projection[0]));
    
    const view = get_camera_matrix(camera);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "view"),
                        1, gl.FALSE, @ptrCast([*]const f32, &view[0]));
    
    const model = [16]f32{
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        transform.position.x, transform.position.y, transform.position.z, 1,
    };
    
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "model"),
                        1, gl.FALSE, @ptrCast([*]const f32, &model[0]));
    
    gl.bindVertexArray(mesh.vao);
    gl.drawElements(gl.TRIANGLES, @intCast(c_int, mesh.i_count), gl.UNSIGNED_INT, null);
    
    assert_gl("Draw Mesh");
}

pub fn draw_solid_rect(rect: Rect, tint: Color, z: f32) void {
    
    gl.useProgram(basic_shader.handle);
    
    gl.uniform4f(gl.getUniformLocation(basic_shader.handle, "tint"),
                 tint.r, tint.g, tint.b, tint.a);
    
    const projection = projection_matrix_from_camera(camera);
    gl.uniformMatrix4fv(gl.getUniformLocation(basic_shader.handle, "projection"),
                        1, gl.FALSE, @ptrCast([*]const f32, &projection[0]));
    
    const view = get_camera_matrix(camera);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(basic_shader.handle, "view"),
                        1, gl.FALSE, @ptrCast([*]const f32, &view[0]));
    
    const model = [16]f32{
        rect.w, 0,      0, 0,
        0,      rect.h, 0, 0,
        0,      0,      1, 0,
        rect.x, rect.y, z, 1,
    };
    
    gl.uniformMatrix4fv(gl.getUniformLocation(basic_shader.handle, "model"),
                        1, gl.FALSE, @ptrCast([*]const f32, &model[0]));
    
    gl.bindVertexArray(quad_vao);
    gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, null);
    
    assert_gl("Draw Solid Rect");
}


pub fn draw_solid_rect_rotate(rect: Rect, angle: f32, tint: Color, z: f32) void {
    gl.useProgram(basic_shader.handle);
    
    gl.uniform4f(gl.getUniformLocation(basic_shader.handle, "tint"),
                 tint.r, tint.g, tint.b, tint.a);
    
    const projection = projection_matrix_from_camera(camera);
    gl.uniformMatrix4fv(gl.getUniformLocation(basic_shader.handle, "projection"),
                        1, gl.FALSE, @ptrCast([*]const f32, &projection[0]));
    
    const view = get_camera_matrix(camera);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(basic_shader.handle, "view"),
                        1, gl.FALSE, @ptrCast([*]const f32, &view[0]));
    
    var model = [16]f32{
        rect.w, 0, 0, 0,
        0, rect.h, 0, 0,
        0, 0, 1, 0,
        -rect.w * 0.5, -rect.h * 0.5, 0, 1,
    };
    
    const model_rot = [16]f32{
        @cos(angle),  @sin(angle), 0, 0,
        -@sin(angle), @cos(angle), 0, 0,
        0, 0, 1, 0,
        rect.x, rect.y, z, 1,
    };
    
    model = Mat4_mul(&model, &model_rot);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(basic_shader.handle, "model"),
                        1, gl.FALSE, @ptrCast([*]const f32, &model[0]));
    
    gl.bindVertexArray(quad_vao);
    gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, null);
    
    assert_gl("Draw Solid Rect");
}

pub fn draw_textured_rect(rect: Rect, texture: Texture, tint: Color, z: f32) callconv(.Inline) void {
    draw_textured_rect_crop(rect, .{}, texture, tint, z);
}

pub fn draw_textured_rect_crop(rect: Rect, crop: Rect, texture: Texture, tint: Color, z: f32) callconv(.Inline) void {
    draw_textured_rect_crop_rotate(rect, crop, 0.0, texture, tint, z);
}

pub fn draw_textured_rect_rotate(rect: Rect, angle: f32, texture: Texture, tint: Color, z: f32) callconv(.Inline) void {
    draw_textured_rect_crop_rotate(rect, .{}, angle, texture, tint, z);
}

pub fn draw_textured_rect_crop_rotate(rect: Rect, crop: Rect, angle: f32, texture: Texture, tint: Color, z: f32) void {
    const shader = texture_shader;
    gl.useProgram(shader.handle);
    
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture.handle);
    
    gl.uniform1i(gl.getUniformLocation(shader.handle, "tex"), 0);
    gl.uniform4f(gl.getUniformLocation(shader.handle, "tint"),
                 tint.r, tint.g, tint.b, tint.a);
    
    gl.uniform4f(gl.getUniformLocation(shader.handle, "crop"),
                 crop.x, crop.y, crop.w, crop.h);
    
    const projection = projection_matrix_from_camera(camera);
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "projection"),
                        1, gl.FALSE, @ptrCast([*]const f32, &projection[0]));
    
    const view = get_camera_matrix(camera);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "view"),
                        1, gl.FALSE, @ptrCast([*]const f32, &view[0]));
    
    
    var model = [16]f32{
        rect.w, 0, 0, 0,
        0, rect.h, 0, 0,
        0, 0, 1, 0,
        -rect.w * 0.5, -rect.h * 0.5, 0, 1,
    };
    
    const model_rot = [16]f32{
        @cos(angle),  @sin(angle), 0, 0,
        -@sin(angle), @cos(angle), 0, 0,
        0, 0, 1, 0,
        rect.x, rect.y, z, 1,
    };
    
    model = Mat4_mul(&model, &model_rot);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "model"),
                        1, gl.FALSE, @ptrCast([*]const f32, &model[0]));
    
    gl.bindVertexArray(quad_vao);
    gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, null);
    
    assert_gl("Draw Crop Rect");
}

pub fn draw_lux_rect_crop_rotate(rect: Rect, crop: Rect, angle: f32, texture: LuxTexture, tint: Color, z: f32) void {
    const shader = lux_shader;
    gl.useProgram(shader.handle);
    
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture.p_handle);
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, texture.m_handle);
    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, texture.a_handle);
    gl.activeTexture(gl.TEXTURE3);
    gl.bindTexture(gl.TEXTURE_2D, texture.n_handle);
    
    gl.uniform1i(gl.getUniformLocation(shader.handle, "pal"), 0);
    gl.uniform1i(gl.getUniformLocation(shader.handle, "tex"), 1);
    gl.uniform1i(gl.getUniformLocation(shader.handle, "att"), 2);
    gl.uniform1i(gl.getUniformLocation(shader.handle, "normalmap"), 3);
    gl.uniform4f(gl.getUniformLocation(shader.handle, "tint"),
                 tint.r, tint.g, tint.b, tint.a);
    
    gl.uniform4f(gl.getUniformLocation(shader.handle, "crop"),
                 crop.x, crop.y, crop.w, crop.h);
    
    const projection = projection_matrix_from_camera(camera);
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "projection"),
                        1, gl.FALSE, @ptrCast([*]const f32, &projection[0]));
    
    const view = get_camera_matrix(camera);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "view"),
                        1, gl.FALSE, @ptrCast([*]const f32, &view[0]));
    
    
    var model = [16]f32{
        rect.w, 0, 0, 0,
        0, rect.h, 0, 0,
        0, 0, 1, 0,
        -rect.w * 0.5, -rect.h * 0.5, 0, 1,
    };
    
    const model_rot = [16]f32{
        @cos(angle),  @sin(angle), 0, 0,
        -@sin(angle), @cos(angle), 0, 0,
        0, 0, 1, 0,
        rect.x, rect.y, z, 1,
    };
    
    model = Mat4_mul(&model, &model_rot);
    
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "model"),
                        1, gl.FALSE, @ptrCast([*]const f32, &model[0]));
    
    gl.bindVertexArray(quad_vao);
    gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, null);
    
    assert_gl("Draw Crop Rect");
}

pub fn pixel_to_world_space(x: f32, y: f32) [2]f32 {
    var result = [2]f32 {x, y};
    
    result[0] = (2.0 * x) / @intToFloat(f32, plt.window.width);
    result[1] = (2.0 * y) / @intToFloat(f32, plt.window.height);
    result[0] = result[0] - 1.0;
    result[1] = 1.0 - result[1];
    
    const m = projection_matrix_from_camera(camera);
    result[0] = result[0] / m[0] + camera.transform.position.x;
    result[1] = result[1] / m[5] + camera.transform.position.y;
    
    return result;
}

pub var font_scale: f32 = 1.0 / 480.0;

pub fn draw_text(text: []const u8, start_x: f32, start_y: f32, font: Font, tint: Color, z: f32) void {
    const shader = font_shader;
    gl.useProgram(shader.handle);
    
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, font.texture.handle);
    gl.bindVertexArray(font_vao);
    
    const projection = projection_matrix_from_camera(camera);
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "projection"),
                        1, gl.FALSE, @ptrCast([*]const f32, &projection[0]));
    
    const view = get_camera_matrix(camera);
    gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "view"),
                        1, gl.FALSE, @ptrCast([*]const f32, &view[0]));
    gl.uniform4f(gl.getUniformLocation(shader.handle, "tint"),
                 tint.r, tint.g, tint.b, tint.a);
    gl.uniform1i(gl.getUniformLocation(shader.handle, "tex"), 0);
    
    const h2w = @intToFloat(f32, plt.window.height) / @intToFloat(f32, plt.window.width);
    
    const ft = font_scale;
    
    var x = start_x / ft;
    var y = start_y / -ft;
    for (text) |t| {
        if (t < font.first_char or t >= font.num_chars + font.first_char) continue;
        
        var q = ztt.get_baked_quad(font.char_data,
                                   t - @intCast(u8, font.first_char), font.texture.width, font.texture.height, &x, &y);
        
        q.x *= ft;
        q.y *= -ft;
        
        q.w *= ft;
        q.h *= -ft;
        
        gl.uniform4f(gl.getUniformLocation(shader.handle, "crop"),
                     q.u, q.v, q.us, q.vs);
        
        const model = [16]f32{
            q.w, 0, 0, 0,
            0, q.h, 0, 0,
            0, 0, 1, 0,
            q.x , q.y, z, 1,
        };
        
        gl.uniformMatrix4fv(gl.getUniformLocation(shader.handle, "model"),
                            1, gl.FALSE, @ptrCast([*]const f32, &model[0]));
        
        
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, null);
    }
    
    assert_gl("Draw Text");
}
