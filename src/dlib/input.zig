usingnamespace @import("dlib.zig");

const KeyState = plt.KeyState;
const Key = plt.Key;

var prev_mouse_pos = [2]f32{0.0, 0.0};
pub var mouse_pos = [2]f32{0.0, 0.0};
pub var mouse_acceleration = [2]f32{0.0, 0.0};
pub var mouse_scroll = [2]f32{0.0, 0.0};

pub const mouse_buttons = enum(u8) {
    left = (1 << 0),
    right = (1 << 1),
    middle = (1 << 2),
};

var key_previus_state = [_]KeyState{.release} ** plt.keymap.len;

pub var mouse_pressed: u8 = 0;
pub var mouse_up: u8 = 0;
pub var mouse_down: u8 = 0;

pub var character_input_queue: [256]u32 = undefined;
pub var character_input_queue_size: usize = 0;

pub var input_is_blocked = false;

pub fn char_input_slice() callconv(.Inline) []const u32 {
    return character_input_queue[0..character_input_queue_size];
}

pub fn poll_input() void {
    character_input_queue_size = 0;
    mouse_down = 0;
    mouse_up = 0;
    mouse_scroll = [_]f32 {0.0, 0.0};
    
    
    for (key_previus_state) |*it, i| {
        it.* = plt.getKeyState(@intToEnum(Key, i));
    }
    
    prev_mouse_pos[0] = mouse_pos[0];
    prev_mouse_pos[1] = mouse_pos[1];
    
    mouse_pos = plt.mousePos();
    
    mouse_acceleration[0] = mouse_pos[0] - prev_mouse_pos[0];
    mouse_acceleration[1] = mouse_pos[1] - prev_mouse_pos[1];
}

pub fn key_down(key: Key) bool {
    const kp = @intCast(usize, @enumToInt(key));
    const previus = key_previus_state[kp];
    const state = plt.getKeyState(key);
    
    var result = false;
    
    if (previus == .release and state == .press) {
        result = !input_is_blocked;
    }
    
    return result;
}

pub fn key_down_no_block(key: Key) bool {
    const kp = @intCast(usize, @enumToInt(key));
    const previus = key_previus_state[kp];
    const state = plt.getKeyState(key);
    
    var result = false;
    
    if (previus == .release and state == .press) {
        result = true;
    }
    
    return result;
}

pub fn key_up(key: Key) bool {
    const kp = @intCast(usize, @enumToInt(key));
    const previus = key_previus_state[kp];
    const state = plt.getKeyState(key);
    
    var result = false;
    
    if (previus == .press and state == .release) {
        result = !input_is_blocked;
    }
    
    return result;
}

pub fn key_pressed(key: Key) bool {
    const result = plt.getKeyState(key) == .press and !input_is_blocked;
    return result;
}

pub fn pressed_as_int(key: Key) i32 {
    return @intCast(i32, @boolToInt(key_pressed(key)));
}

pub fn pressed_as_float(key: Key) f32 {
    return @intToFloat(f32, @boolToInt(key_pressed(key)));
}

pub fn mouse_button_down(button: mouse_buttons) bool {
    return (@enumToInt(button) & mouse_down) == @enumToInt(button);
}

pub fn mouse_button_up(button: mouse_buttons) bool {
    return (@enumToInt(button) & mouse_up) == @enumToInt(button);
}

pub fn mouse_button_pressed(button: mouse_buttons) bool {
    return (@enumToInt(button) & mouse_pressed) == @enumToInt(button);
}
