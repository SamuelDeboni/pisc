#version 330 core
out vec4 FragColor;
in vec3 f_pos;

uniform vec4 tint;

void main()
{
    FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f) * tint;
}
