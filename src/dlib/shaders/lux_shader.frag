#version 330 core
out vec4 FragColor;

in vec3 f_pos;
in vec2 f_uv;
in vec3 f_normal;

uniform highp mat4 model;

uniform vec4 tint;
uniform sampler2D pal;
uniform sampler2D tex;
uniform sampler2D att;
uniform sampler2D normalmap;


void main()
{
    vec4 mt          = texture(tex, f_uv);
    vec3 normal      = texture(normalmap, f_uv).xyz;
    
    normal.x = (normal.x - 0.5) * 2.0;
    normal.y = (normal.y - 0.5) * 2.0;
    normal.z = (normal.z - 0.5) * 2.0;
    
    normal = mat3(model) * normal;
    
    normal = normalize(normal);
    
    float atenuation = texture(att, f_uv).r;
    float specular_strenght = texelFetch(pal, ivec2(0, mt.r * 255), 0).r;
    
    vec3 light_dir  = vec3(0, 2, -1);
    light_dir       = normalize(light_dir);
    float dot_p     = dot(normal, light_dir);
    vec3 reflection = reflect(light_dir, normal);
    
    ivec2 pal_size = textureSize(pal, 0);
    
    float i_difuse   = ((pal_size.x - 3) * 0.5) * dot_p;
    float i_specular = (pal_size.x - 4) * reflection.z * specular_strenght;
    
    i_difuse = floor(i_difuse)   + (fract(i_difuse)   > 0.5 ? 1.0 : 0.0);
    i_difuse = floor(i_specular) + (fract(i_specular) > 0.5 ? 1.0 : 0.0);
    
    i_difuse   += 2;
    i_specular += 2;
    
    i_difuse   *= atenuation;
    i_specular *= atenuation;
    
    int index = int(i_difuse > i_specular ? i_difuse : i_specular);
    index = clamp(index, 0, int(pal_size - 1));
    
    ivec2 p_uv   = ivec2(index + 1, mt.r * 255);
    vec4 f_color = texelFetch(pal, p_uv, 0);
    
    FragColor = f_color;
    
    //FragColor = vec4(atenuation, atenuation, atenuation, 1)
}
