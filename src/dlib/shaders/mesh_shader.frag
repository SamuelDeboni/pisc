#version 330 core
out vec4 FragColor;

in vec3 f_pos;
in vec2 f_uv;
in vec3 f_normal;

uniform vec4 tint;
uniform sampler2D tex;

vec2 normalizeUv(sampler2D t, vec2 uv, float aaf) {
    vec2 ruv = uv;
    vec2 res = textureSize(t, 0);
    ruv = ruv * res + 0.5;
    
    // tweak fractionnal value of the texture coordinate
    vec2 fl = floor(ruv);
    vec2 fr = fract(ruv);
    vec2 aa = fwidth(ruv) * aaf;
    fr = smoothstep( vec2(0.5,0.5) - aa, vec2(0.5,0.5) + aa, fr);
	ruv = (fl + fr - 0.5) / res;
    
    return ruv;
}


void main()
{
    vec3 norm = normalize(f_normal);
    vec3 light_dir = normalize(vec3(0.3, 0.0, 0.6));
    float dif = max(dot(norm, light_dir), 0.5);
    //dif = 1.0;
    float voxel = abs(norm.x) * 0.6 + abs(norm.y) * 0.8 + abs(-norm.z);
    //voxel = 1.0;
    
    vec3 light_color = vec3(1, 1, 1);
    float ambient = 1.0;
    vec3 result = (dif * 0.6 + voxel * 0.4) * light_color * ambient;
    
    vec2 nuv = normalizeUv(tex, f_uv, 0.5);
    
    FragColor = texture(tex, nuv) * vec4(result, 1.0);
}
