#version 330 core

layout (location = 0) in vec3 aPos;

out vec3 f_pos;
out vec2 f_uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec4 crop;

void main()
{
	vec2 cord;
    cord.y = crop.y + crop.w * float(gl_VertexID == 0 || gl_VertexID == 3);
    
	cord.x = crop.x + crop.z * (gl_VertexID / 2);
    
    f_uv = cord;
    
    vec4 position = view * model * vec4(aPos.x, aPos.y, aPos.z, 1.0);
    f_pos = position.xyz;
    gl_Position = projection * position;
}
