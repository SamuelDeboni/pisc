// Change this file to integrate with your code
usingnamespace @import("dlib.zig");
usingnamespace @import("duilib.zig");

pub const Font = render.Font;

// Draw Functions
pub fn draw_rect(rect: UiRect, color: UiColor) void {
    render.draw_solid_rect(Rect.c(rect.x, rect.y, rect.w, rect.h),
                           Color.c(color.r, color.g, color.b, color.a), 1);
}

pub fn drawRectZ(rect: UiRect, color: UiColor, z: f32) void {
    render.draw_solid_rect(Rect.c(rect.x, rect.y, rect.w, rect.h),
                           Color.c(color.r, color.g, color.b, color.a), z);
}

const TextRenderQueueEntry = struct {
    t: []const u8,
    x: f32,
    y: f32,
    font: Font,
    color: UiColor,
};

var text_render_queue: [512]TextRenderQueueEntry = undefined;
var text_render_queue_count: usize = 0;

fn drawTextQueue() void {
    for (text_render_queue[0..text_render_queue_count]) |it| {
        render.draw_text(it.t, it.x, it.y, it.font, Color.c(it.color.r, it.color.g, it.color.b, it.color.a), 1);
    }
    text_render_queue_count = 0;
}

pub fn draw_text(al: *Allocator, t: []const u8, x: f32, y: f32, font: Font, color: UiColor) void {
    
    if (text_render_queue_count < text_render_queue.len) {
        var tmp: []u8 = al.alloc(u8, t.len) catch return;
        for (tmp) |*it, i| it.* = t[i];
        
        text_render_queue[text_render_queue_count] = .{
            .t = tmp,
            .x = x,
            .y = y,
            .font = font,
            .color = color,
        };
        
        text_render_queue_count += 1;
    }
}


pub fn init(al: *Allocator, font_h: f32) !UiContext {
    var result = UiContext{};
    const noto_sans_ttf = @embedFile("../assets/NotoSans-Regular.ttf");
    result.font = try render.bake_font(al, font_h, 32, 96, noto_sans_ttf);
    result.font_h = font_h * (1.0 / 1920.0);
    
    var buf = try al.alloc(u8, 1024 * 1024);
    result.al = std.heap.FixedBufferAllocator.init(buf);
    
    return result;
}

pub fn deinit(al: *Allocator, ctx: *UiContext) void {
    render.delete_texture(ctx.font.texture);
    al.free(ctx.font.char_data);
    al.free(ctx.al.buffer);
}

pub fn start(ctx: *UiContext) void {
    ctx.mouse_pos = render.pixel_to_world_space(input.mouse_pos[0], input.mouse_pos[1]);
    ctx.mouse_up   = if (plt.cursor_is_enabled) input.mouse_button_up(.left) else false;
    ctx.mouse_down = if (plt.cursor_is_enabled) input.mouse_button_down(.left) else false;
    const w_size = render.pixel_to_world_space(cast(f32, plt.window.width),
                                               cast(f32, plt.window.height));
    ctx.width = w_size[0] * 2.0;
    ctx.height = w_size[1] * -2.0;
}

pub fn end(ctx: *UiContext) void {
    drawTextQueue();
    ctx.al.reset();
}